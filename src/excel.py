#!/usr/bin/python

import xlwt
from collections import defaultdict
import directories as dirs
import operator
import time
import re

# Constants
LPS     = 0
CPS     = 1
STREAMS = 2
VOLUMES = 3
TOTAL_V = 4

PROFIT  = 0
VOLUME  = 1
TICKETS = 2

PROFIT_DIV = 1000000

YEAR    = -1
MONTH   = -2
DAY     = -3

MONTH_LEN = 4
DAY_LEN   = 5

LP_NAME     = 0
LP_STREAM   = 1
C_PAIR      = 2
C_VOL       = 3
STREAM_VOL  = 4
PERCENT     = 5
S_TICKETS   = 6
AVG_TICKET  = 7

CHAR_PIX   = 256

# Month map
month_map = {
             "Jan": "01-Jan", "Feb": "02-Feb", "Mar": "03-Mar",
             "Apr": "04-Apr", "May": "05-May", "Jun": "06-Jun",
             "Jul": "07-Jul", "Aug": "08-Aug", "Sep": "09-Sep",
             "Oct": "10-Oct", "Nov": "11-Nov", "Dec": "12-Dec"
            }

path = dirs.root + "Yield Analysis/stats/"

# Dictionary containing all LPs. Each LP maps to a:
#   - Currency Pair
#   - Total Volume
#   - LP Name
#   - Stream Name
#   - Stream Volume
#   - Stream Volume Percentage
#   - Number of Tickets
#   - Average Ticket Size
#   - Client Stream
reports = {}

def client(org):
    if org == "GPFXMT4":
        return "MT4"
    elif org == "MST":
        return "HF"
    elif org == "YIB":
        return "Metals"

    return org

# Generate LP reports
def generate_lp_report(f_name):
    year  = f_name.split("-")[YEAR].replace(".csv", "")
    month = f_name.split("-")[MONTH]

    for lp in reports.keys():
        for org, stats in reports[lp].iteritems():
            if org not in ("all", "MST"): continue
            book = xlwt.Workbook()

            currency_fmt = xlwt.XFStyle()
            currency_fmt.num_format_str = "#,##0.00"
            comma_fmt = xlwt.easyxf("", "#,##0")
            percent_fmt = xlwt.easyxf(num_format_str='0.00')
            title = xlwt.easyxf( 'font: bold 1, height 280; pattern: pattern solid, fore_colour light_blue;' )
            heading = xlwt.easyxf( 'font: bold 1; align: wrap 1;' )

            sheet = book.add_sheet("Monthly Report")

            sheet.write(0, 0, "Gleneagle Securities", title)
            sheet.write(1, 0, "FX Stats " + month + " " + year, title)
            sheet.write(0, 1, "", title)
            sheet.write(0, 2, "", title)
            sheet.write(1, 1, "", title)
            sheet.write(1, 2, "", title)

            sheet.write(2, 0, "LP", heading)
            sheet.write(2, 1, "Stream", heading)
            sheet.write(2, 2, "Currency\nPair", heading)
            sheet.write(2, 3, "Total Pair\nVolume", heading)
            sheet.write(2, 4, "Stream Volume", heading)
            sheet.write(2, 5, "Percentage\nof Total", heading)
            sheet.write(2, 6, "Tickets", heading)
            sheet.write(2, 7, "Average\nTicket Size", heading)

            # Adjust column widths
            sheet.col(C_PAIR).width = CHAR_PIX * 10
            sheet.col(C_VOL).width = CHAR_PIX * 14
            sheet.col(STREAM_VOL).width = CHAR_PIX * 14
            sheet.col(PERCENT).width = CHAR_PIX * 14
            sheet.col(AVG_TICKET).width = CHAR_PIX * 11

            s_stats = sorted(stats, key=operator.itemgetter(C_PAIR))

            row = 3
            for s in s_stats:
                sheet.write(row, LP_NAME,    s[LP_NAME])
                sheet.write(row, LP_STREAM,  s[LP_STREAM])
                sheet.write(row, C_PAIR,     s[C_PAIR])
                sheet.write(row, C_VOL,      s[C_VOL], comma_fmt)
                sheet.write(row, STREAM_VOL, s[STREAM_VOL], comma_fmt)
                sheet.write(row, PERCENT,    s[PERCENT], percent_fmt)
                sheet.write(row, S_TICKETS,  s[S_TICKETS], comma_fmt)
                sheet.write(row, AVG_TICKET, s[AVG_TICKET], comma_fmt)
                row += 1

            rec = "/home/adrian/host/Box Sync/Global Prime FX/"\
                  + "Global Prime - Forex/FX Stats/FX Stats/"

            path = rec + "/" + year[-2:] + month_map[month][0:2]
            path += "/Reports/" + lp + "-" + client(org) + ".xls"

            book.save(path)

# Helper function to rename the out file
def rename(name):
    # remove the .csv extention if there
    parts = name.replace(".csv", "")

    parts = parts.split("-")

    day   = parts[DAY] + "-" if len(parts) == DAY_LEN else ""
    month = parts[MONTH]
    year  = parts[YEAR]

    return year + "/" + month_map[month] + "/" + day + month + \
           ".xls"

# Fill LP reports with remaining currency pairs that have no
# volume
def fill_reports(volumes):
    for lp, orgs in reports.iteritems():
        for org, stats in orgs.iteritems():
            # Setify currency pairs from stats and volumes
            # and then take the set difference
            rep = set([s[C_PAIR] for s in stats])
            vol = set([k for k in volumes[org]])
            dif = vol - rep
            print lp, org, dif

            # Now add the volumes and pairs to stats
            for pair in dif:
                data = []
                data.append(lp)
                data.append('')
                data.append(pair)
                data.append(volumes[org][pair])
                data.append(0)
                data.append(0)
                data.append(0)
                data.append(0)

                reports[lp][org].append(data)
            # print data


def excel(f_name, buckets, rep):
    book = xlwt.Workbook()

    currency_fmt = xlwt.XFStyle()
    currency_fmt.num_format_str = "#,##0.00"
    comma_fmt = xlwt.easyxf("", "#,###")
    percent_fmt = xlwt.easyxf(num_format_str='0.00')

    # LP sheet
    sheet = book.add_sheet("LPs")

    row = 0
    sheet.write(row, 0, "Name")
    sheet.write(row, PROFIT+1, "Yield")
    sheet.write(row, VOLUME+1, "Volume")
    sheet.write(row, 3, "Tickets")
    sheet.write(row, 4, "Average Ticket Size")

    # Sort LP names alphabetically by name. Secondary key is ID
    lps = sorted(buckets[LPS].items(), key=operator.itemgetter(0))

    row += 1
    for lp in lps:
        sheet.write(row, 0, lp[0])
        # print lp[1][PROFIT]
        sheet.write(row, PROFIT+1, lp[1][PROFIT]/(lp[1][VOLUME]/PROFIT_DIV), style=currency_fmt)
        sheet.write(row, VOLUME+1, lp[1][VOLUME], style=comma_fmt)
        sheet.write(row, 3, lp[1][TICKETS], comma_fmt)
        avg_ticket = lp[1][VOLUME]/lp[1][TICKETS] if lp[1][TICKETS] > 0 else 0
        sheet.write(row, 4, avg_ticket, style=comma_fmt)
        row += 1

    # CP sheet
    sheet = book.add_sheet("Counter Parties")

    row = 0
    sheet.write(row, 0, "ID")
    sheet.write(row, PROFIT+1, "Yield")
    sheet.write(row, VOLUME+1, "Volume")
    sheet.write(row, 3, "Tickets")
    sheet.write(row, 4, "Average Ticket Size")

    # Sort CPs by ID. Secondary key is name
    cps = sorted(buckets[CPS].items(), key=operator.itemgetter(0))

    row += 1
    for cp in cps:
        sheet.write(row, 0, cp[0])
        sheet.write(row, PROFIT+1, cp[1][PROFIT]/(cp[1][VOLUME]/PROFIT_DIV), style=currency_fmt)
        sheet.write(row, VOLUME+1, cp[1][VOLUME], style=comma_fmt)
        sheet.write(row, 3, cp[1][TICKETS], comma_fmt)
        avg_ticket = cp[1][VOLUME]/cp[1][TICKETS] if cp[1][TICKETS] > 0 else 0
        sheet.write(row, 4, avg_ticket, style=comma_fmt)
        row += 1

    # Streams sheets
    for k, v in buckets[STREAMS].iteritems():
        if k == "all":
            sheet = book.add_sheet("Streams")
        else :
            sheet = book.add_sheet("Streams | " + k)

        row = 0
        sheet.write(row, 0, "Name")
        sheet.write(row, PROFIT+1, "Yield")
        sheet.write(row, VOLUME+1, "Volume")
        sheet.write(row, 3, "Tickets")
        sheet.write(row, 4, "Average Ticket Size")

        # Sort streams by name
        streams = sorted(v.items(), key=operator.itemgetter(0))

        row += 1
        for stream in streams:
            sheet.write(row, 0, stream[0])
            sheet.write(row, 1, stream[1][PROFIT]/(stream[1][VOLUME]/PROFIT_DIV), style=currency_fmt)
            sheet.write(row, 2, stream[1][VOLUME], style=comma_fmt)
            sheet.write(row, 3, stream[1][TICKETS], comma_fmt)
            avg_ticket = stream[1][VOLUME]/stream[1][TICKETS] if stream[1][TICKETS] > 0 else 0
            sheet.write(row, 4, avg_ticket, style=comma_fmt)
            row += 1

    # Currency volumes sheets
    for org, cp in buckets[VOLUMES].iteritems():
        if org == "all":
            sheet = book.add_sheet("Currency Volumes")
        else:
            sheet = book.add_sheet("Currency Volumes | " + org)

        row = 0
        sheet.write(row, 0, "Currency Pairs")
        sheet.write(row, 1, "Currency Volume")
        sheet.write(row, 2, "LP")
        sheet.write(row, 3, "Stream")
        sheet.write(row, 4, "Stream Yield")
        sheet.write(row, 5, "Stream Volume")
        sheet.write(row, 6, "Volume Percentage")
        sheet.write(row, 7, "Tickets")
        sheet.write(row, 8, "Average Ticket Size")

        # Sort volume totals
        totals = sorted(buckets[TOTAL_V][org].items(), key=operator.itemgetter(1), reverse=True)

        row += 1
        for pair, vol in totals:

            # Sort the rest by stream volume
            lps = sorted(cp[pair].iteritems(), key=operator.itemgetter(1), reverse=True)

            for lp in lps:
                # print pair, row
                name   = lp[0][0]
                stream = lp[0][1]

                sheet.write(row, 0, pair)
                sheet.write(row, 1, vol, style=comma_fmt)
                sheet.write(row, 2, name)
                sheet.write(row, 3, stream)

                profit     = lp[1][PROFIT]
                volume     = lp[1][VOLUME]
                tickets    = lp[1][TICKETS]
                _yield     = profit / (volume / PROFIT_DIV)
                percent    = (volume / vol) * 100
                avg_ticket = volume / tickets

                sheet.write(row, 4, _yield, style=currency_fmt)
                sheet.write(row, 5, volume, style=comma_fmt)
                sheet.write(row, 6, percent, style=percent_fmt)
                sheet.write(row, 7, tickets, style=comma_fmt)
                sheet.write(row, 8, avg_ticket, style=comma_fmt)

                row += 1

                if not rep: continue

                # Add to report
                if name not in reports:
                    reports[name] = {}
                lp_report = reports[name]
                
                if org not in reports[name]:
                    reports[name][org] = []

                data = []
                data.append(name)
                data.append(stream)
                data.append(pair)
                data.append(vol)
                data.append(volume)
                data.append(percent)
                data.append(tickets)
                data.append(avg_ticket)

                reports[name][org].append(data)


    if rep:
        # Fill remaining currency pairs for lp report
        fill_reports(buckets[TOTAL_V])
        generate_lp_report(f_name)

    f_dir = path + rename(f_name)
    # print f_dir
    book.save(f_dir)
    