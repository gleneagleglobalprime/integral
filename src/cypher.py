import string
from random import shuffle

class Ceasar:
    def __init__(self, shift):
        self.shift = shift

        self.base = [c for c in string.ascii_lowercase]

        self.encypher = {}
        self.decypher = {}
        for i in range(len(self.base)):
            self.encypher[self.base[i]] = self.base[(i+self.shift) % 26]
            self.decypher[self.base[i]] = self.base[(i-self.shift) % 26]

        self.encypher[' '] = ' '
        self.decypher[' '] = ' '

    def encrypt(self, msg):
        return "".join([self.encypher[m] for m in msg.lower()])

    def decrypt(self, msg):
        return "".join([self.decypher[m] for m in msg.lower()])

class Vernam:
    def __init__(self):
        # self.alphabet = [chr(x) for x in range(128)]
        pass

    def gen_key(self, msg):
        key = range(len(msg))
        shuffle(key)
        return [x % 128 for x in key]

    def encrypt(self, msg):
        key = self.gen_key(msg)
        msg_o = [ord(x) for x in msg]
        new = map(lambda x, y: x + y % 128, key, msg_o)
        return "".join([chr(x) for x in new]), key

    def decrypt(self, msg, key):
        msg_o = [ord(x) for x in msg]
        old = map(lambda x, y: x - y % 128, msg_o, key)
        return "".join([chr(x) for x in old])



# c = Ceasar(1)

# print c.encrypt("z")
# print c.encrypt("abcde")
# print c.encrypt("poop")
# print c.decrypt("abcde")
# print c.decrypt(c.encrypt("poop"))
# print c.decrypt(c.encrypt("".join([c for c in string.ascii_lowercase])))

v = Vernam()
# msg, key = v.encrypt("abcefghijklmnop")
# print msg, key
# print v.decrypt(msg, key)

# with open ("les_mis", "r") as myfile:
#     msg = myfile.read().decode('utf-8').encode('ascii', 'replace')
# en, key = v.encrypt(msg)
# de = v.decrypt(en, key)
# print de
# print msg
# print de == msg

def evens(l):
    return len([n for n in l if n % 2 == 0])

def big_diff(l):
    return max(l) - min(l)

def c_avg(l):
    l.remove(max(l))
    l.remove(min(l))
    return sum(l)/len(l)

def sum13(l):
    return sum(n for n in l if n != 13 and n != 14)

def make_bricks(small, big, goal):
    print small, big, goal
    if goal == 0:
        return True
    if big > 0 and goal - 5 >= 0:
        return make_bricks(small, big-1, goal-5)
    elif small > 0 and goal - 1 >= 0:
        return make_bricks(small-1, big, goal-1)
    else:
        return False

def lone_sum(l):
    pass


print evens(range(100))
print big_diff(range(10))
print c_avg([1, 2, 3, 4, 100])
print sum13([1, 2, 2, 1, 13])
print make_bricks(0, 1, 5)
print make_bricks(0, 2, 5)
print make_bricks(0, 2, 10)
print make_bricks(0, 2, 9)
print make_bricks(0, 2, 11)
print make_bricks(1, 2, 11)
print make_bricks(1, 2, 9)
print make_bricks(1, 1, 1)