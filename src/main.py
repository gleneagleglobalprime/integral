import directories as dirs
import os
import glob
import time
import match
import bucket
import excel
import sys
import getopt

inn = dirs.root + "Yield Analysis/in/"
err = dirs.root + "Yield Analysis/error/"
out = dirs.root + "Yield Analysis/out/"

suffix = ".csv"

def main(argv):
    report = False
    try:
        opts, args = getopt.getopt(argv,"hr",["report"])
    except getopt.GetoptError:
        print 'main.py -r'
        sys.exit(2)
    for opt, arg in opts:
       if opt == '-h':
           print 'main.py [-r]'
           sys.exit()
       elif opt in ("-r", "--report"):
           report = True

    while True:
        files = glob.glob(inn + "*")
        
        # Take first available file and inspect
        if files:
            f_dir  = files[0]
            f_name = f_dir.split("/")[-1]

            print "Looking at \"{}\"".format(f_name)
            if not f_dir.endswith(suffix):
                # Move if not a .csv file
                print "{} is not a .csv, moving into error directory.".\
                      format(f_name)
                os.rename(f_dir, err + f_name)

            else:
                # Begin processing
                # Match takers and makers and store in a
                # Trade class
                print "Matching up trades.."
                trades  = match.out(f_dir)
                print "Calculating volumes and yields.."
                buckets = bucket.out(trades)
                print "Writing to excel"
                excel.excel(f_name, buckets, report)
                # break

                # Finally, complete. Move files
                print "Done! Moving {} into out directory.".format(f_name)
                os.rename(f_dir, out + f_name)

        
        # Wait...
        time.sleep(10)

if __name__ == "__main__":
    main(sys.argv[1:])