import datetime
import directories as dirs
from   hohenheim import *
import sys
import xlrd

DATA   = 0
MONDAY = 0

HARMONY_ID = 0
TRADE_DATE = 4
LP         = 13
PAIR       = 23
COST       = 24

LPS = {"BNP EB":                "BNPP",
       "Bank of America":       "BOAN",
       "Barclays EB":           "BARX",
       "Credit Suisse EB":      "CRSU",
       "DBS":                   "DB",
       "Goldman Sachs EB":      "GSFX",
       "JPMC_EB":               "JPM",
       "Morgan Stanley EB":     "MSFX",
       "RBS EB":                "RBS",
       "UBS EB":                "UBS",
       "Virtu Financial LLC":   "VTFX"
       }



def push(session):
    yesterday = datetime.datetime.today() - datetime.timedelta(1)
    yesterday = yesterday.replace(hour=0, minute=0, second=0, microsecond=0)

    # If today is a Monday, last aggregation day was Friday
    if datetime.datetime.today().weekday() == MONDAY:
        yesterday -= datetime.timedelta(2)

    month = str(yesterday.month).zfill(2)
    year  = str(yesterday.year)[2:]

    path = dirs.root + "Aggregation/{}_Pivot Table_Tickets.xlsx"
    path  = path.format(year + month)

    book = xlrd.open_workbook(path)
    sheet = book.sheet_by_index(DATA)

    # print sheet.ncols, sheet.nrows
    date_string = yesterday.strftime('%-m/%d/%Y')

    inserts = []

    for row in range(1, sheet.nrows):
        date = sheet.cell(row, TRADE_DATE).value
        if date != date_string: continue
        # if date not in ("5/22/2015", "5/25/2015"): continue

        h_id = sheet.cell(row, HARMONY_ID).value
        name = LPS[sheet.cell(row, LP).value]
        pair = sheet.cell(row, PAIR).value.replace("-", "/")
        cost = sheet.cell(row, COST).value

        date_parts = date.split("/")
        formatted_date = date_parts[2] + "-" + date_parts[0] + "-" +\
                         date_parts[1]

        lp = session.query(LPs).\
             filter(LPs.name == name).first()

        if not lp:
            lp = LPs(name=name)
            session.add(lp)

        costs = session.query(AggregationCosts).\
                filter(AggregationCosts.harmony_id == h_id).first()

        if not costs:
            cost = float(cost) if cost else None
            costs = AggregationCosts(lp=lp.id, harmony_id=h_id,
                                     trade_date=formatted_date,
                                     c_pair=pair, cost=cost)

            inserts.append(costs)

    print "costs:", len(inserts)
    session.add_all(inserts)
    session.commit()

