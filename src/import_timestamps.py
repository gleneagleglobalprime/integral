#!/usr/bin/python
import csv
import datetime
import directories as dirs
import getopt
import glob
import match
import os
import rejections
import sys
import time
from utils import connect
import utils.date as ud

inn = dirs.root + "Yield Analysis/in/"
err = dirs.root + "Yield Analysis/error/"
out = dirs.root + "Yield Analysis/out/"

suffix = ".csv"

def main(argv):
    files = glob.glob(inn + "*")
    rejections = []
    
    # # Take first available file and inspect
    if files:
        for f in files:
            f_name = f.split("/")[-1]

            print "Looking at \"{}\"".format(f_name)
            if not f.endswith(suffix):
                # Move if not a .csv file
                print "{} is not a .csv, moving into error directory."\
                      .format(f_name)
                os.rename(f, err + f_name)

            else:
                trades = csv.reader(open(f, 'rU'), delimiter=",", quotechar='"')
                rejections += [(r[2], r[9], r[11]) for r in trades if r[0] == "R"]

    # Push new data
    conn, cur = connect.psql()
    # q = "select trade, spot * term_amt from takers"
    # cur.execute(q)
    # trades = {}
    # for trade, mult in cur.fetchall():
    #     if trade in trades:
    #         trades[trade] += mult
    #     else:
    #         trades[trade] = mult

    count = 0
    total = len(rejections)
    for tid, stamp, side in rejections:
        q = "update rejections set timestamp='{0}', buy_sell='{1}' where trade_id='{2}'".format(stamp, side, tid)
        cur.execute(q)
        print count, total
        count +=1

    conn.commit()
    conn.close()

    # for fxi, timestamp in timestamps:
    #     try:
    #         q = "update trades as t set timestamp='{0}' where maker={1}".format(timestamp, makers[fxi])
    #         cur.execute(q)
    #     except KeyError:
    #         pass

    #     print timestamp

        # q = "select * from trades where timestamp='{0}'".format(timestamp)
        # cur.execute(q)

    # sys.exit()

if __name__ == "__main__":
    main(sys.argv[1:])