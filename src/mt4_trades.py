#!/usr/bin/python
import csv
import datetime
import directories as dirs
import getopt
import glob
import os
import sys
import time
import utils
import utils.date as ud

inn = dirs.root + "Yield Analysis/in/"
err = dirs.root + "Yield Analysis/error/"
out = dirs.root + "Yield Analysis/out/"

suffix = ".csv"