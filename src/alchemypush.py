import os
import re
import sys
from datastructs import *
from hohenheim import *
import costs
import utils.connect
import rolls.rolls as rr
import sqlalchemy as sa

MAKER = 0
TAKER = 1

# Set up connection to postgres via SQLAlchemy
Base.metadata.bind = engine
DBSession = sa.orm.sessionmaker(bind=engine)
session = DBSession()

conn, curr = utils.connect.psql()
data = {}

def load_data():
    global curr, data

    data = {'cps':        {},
            'lps':        {},
            'accounts':   {},
            'streams':    {},
            'makers':     {},
            'trades':     {},
            'takers':     {},
            'rejections': {}
            }

    print "Loading cps, lps, accounts and streams..."
    q = "select id, name from cps"
    curr.execute(q)
    for row in curr.fetchall(): data['cps'][row[1].strip()] = row[0]

    q = "select id, name from lps"
    curr.execute(q)
    for row in curr.fetchall(): data['lps'][row[1].strip()] = row[0]

    q = "select id from accounts"
    curr.execute(q)
    for row in curr.fetchall(): data['accounts'][row[0]] = None

    q = "select id, name from streams"
    curr.execute(q)
    for row in curr.fetchall(): data['streams'][row[1]] = row[0]

    print "Loading trades..."
    q = "select id, maker from trades"
    curr.execute(q)
    for row in curr.fetchall(): data['trades'][row[1]] = row[0]

    print "Loading makers..."
    q = "select id, trade_id from makers"
    curr.execute(q)
    for row in curr.fetchall(): data['makers'][row[1]] = row[0]

    print "Loading takers..."
    q = "select id, trade_id from takers"
    curr.execute(q)
    for row in curr.fetchall(): data['takers'][row[1]] = row[0]

    print "Loading rejections..."
    q = "select id, trade_id from rejections"
    curr.execute(q)
    for row in curr.fetchall(): data['rejections'][row[1]] = row[0]

def push_trades(raw_trades):
    global session, curr, conn, data
    
    count = 0
    for _trade in raw_trades:
        inserts = []

        if count % 100 == 0: print count
        # if count % 10000 == 0: session.commit()
        
        t = Trade(_trade[MAKER], _trade[TAKER])
        if not t.takers:
            continue
        print t.maker.profit
        # if t.maker.profit>1000 or t.maker.profit <0:
        #     print t.maker.trade_id
        #     sys.exit()

        # Get or create CP
        if t.maker.name in data['cps']:
            cp = data['cps'][t.maker.name]
        else:
            q = "insert into cps values (DEFAULT, '{}') returning id"
            q = q.format(t.maker.name)

            curr.execute(q)
            cp = curr.fetchone()[0]
            data['cps'][t.maker.name] = cp
        # print cp.name.strip(), cp.id

        # Get or create stream
        if t.maker.name.strip() == "GPFXMT4":
            a_id = t.maker.id
        else:
            a_id = t.maker.stream

        if a_id in data['accounts']:
            account = a_id
        else:
            q = "insert into accounts values ('{}', {}) returning id"
            q = q.format(a_id, cp)

            # print a_id, cp
            curr.execute(q)
            account = curr.fetchone()[0]
            data['accounts'][account] = None
        # print account.id, account.cp

        # Get or create maker
        if t.maker.trade_id in data['makers']:
            maker = data['makers'][t.maker.trade_id]
        else:
            rate = float(t.maker.rate) if t.maker.rate else "null"

            q = "insert into makers values (DEFAULT, {}, '{}', {}, {}, " +\
                "{}, {}, {}, '{}', {}, '{}', '{}') returning id"
            # print cp, t.maker.type, float(t.maker.spot), float(t.maker.base), float(t.maker.term), rate, float(t.maker.volume), t.maker.trade_id, t.maker.profit, account, None
            # print t.takers
            q = q.format(cp, t.maker.type, float(t.maker.spot),
                         float(t.maker.base), float(t.maker.term), rate,
                         float(t.maker.volume), t.maker.trade_id,
                         float(t.maker.profit), account, None)

            curr.execute(q)
            maker = curr.fetchone()[0]
            data['makers'][t.maker.trade_id] = maker
        # print maker.id, maker.trade_id, maker.cp, maker.account

        # Get or create trade
        if maker in data['trades']:
            trade = data['trades'][maker]
        else:
            print t.maker.trade_id
            print t.fillprice
            print t.fillsize
            print t.base_fillsize
            q = "insert into trades values (DEFAULT, {}, '{}', '{}', " +\
                "'{}', '{}', '{}', '{}', '{}') returning id"
            q = q.format(maker, t.maker.pair, t.maker.trade_date,
                         t.maker.value_date, t.maker.timestamp,
                         t.fillprice, t.fillsize, t.base_fillsize)

            curr.execute(q)
            trade = curr.fetchone()[0]
            data['trades'][maker] = trade
        # print trade.id, trade.maker, trade.c_pair

        if t.takers != None:
            for tt in t.takers:
                # Get or create lp
                if tt.name in data['lps']:
                    lp = data['lps'][tt.name]
                else:
                    q = "insert into lps values (DEFAULT, '{}') returning id"
                    q = q.format(tt.name)

                    curr.execute(q)
                    lp = curr.fetchone()[0]
                    data['lps'][tt.name] = lp
                # print lp.id, lp.name

                # Get or create stream
                if tt.stream in data['streams']:
                    stream = data['streams'][tt.stream]
                else:
                    q = "insert into streams values (DEFAULT, '{}', {}) " +\
                        "returning id"
                    q = q.format(tt.stream, lp)

                    curr.execute(q)
                    stream = curr.fetchone()[0]
                    data['streams'][tt.stream] = stream
                # print stream.id, stream.name, stream.lp

                # Get or create taker
                if tt.trade_id not in data['takers']:
                    rate = float(tt.rate) if tt.rate else "null"

                    q = "insert into takers values (DEFAULT, {}, '{}', {}, " +\
                        "{}, {}, {}, {}, {}, '{}', {}, '{}')"
                    q = q.format(lp, tt.type, float(tt.spot),
                                 float(tt.term), rate, float(tt.volume),
                                 float(tt.profit), stream, tt.trade_id,
                                 trade, None)
                    curr.execute(q)

        # if len(t.takers) > 4:
        #     sys.exit()
        count += 1

    conn.commit()

def push_rejections(raw_rejections):
    global session

    inserts = []
    count = 0
    for rej in raw_rejections:
        if count % 100 == 0: print count

        if re.match( r'^AHL\d+', rej[MAKER][CLIENT].strip(), re.M ):
            rej[MAKER][CLIENT] = "AHL"

        if rej[MAKER][CLIENT] in data['cps']:
            cp = data['cps'][rej[MAKER][CLIENT]]
        else:
            q = "insert into cps values (DEFAULT, {}) returning id"
            q = q.format(rej[MAKER][CLIENT])

            curr.execute(q)
            cp = curr.fetchone()[0]
            data['cps'][rej[MAKER][CLIENT]] = cp

        if rej[MAKER][CLIENT].strip() == "GPFXMT4":
            a_id = rej[MAKER][CLIENT_ID]
        else:
            a_id = rej[MAKER][STREAM]

        if a_id in data['accounts']:
            account = a_id
        else:
            q = "insert into accounts values ('{}', {}) returning id"
            q = q.format(a_id, cp)

            curr.execute(q)
            account = curr.fetchone()[0]
            data['accounts'][account] = None

        # Taker not required to be in a rejection
        if rej[TAKER]:
            if rej[TAKER][CLIENT] in data['lps']:
                lp = data['lps'][rej[TAKER][CLIENT]]
            else:
                q = "insert into lps values (DEFAULT, '{}') returning id"
                q = q.format(rej[TAKER][CLIENT])

                curr.execute(q)
                lp = curr.fetchone()[0]
                data['trades'][rej[TAKER][CLIENT]] = lp

            if rej[TAKER][STREAM] in data['streams']:
                stream = data['streams'][rej[TAKER][STREAM]]
            else:
                q = "insert into streams values (DEFAULT, '{}', {}) " +\
                    "returning id"
                q = q.format(rej[TAKER][STREAM], lp)

                curr.execute(q)
                stream = curr.fetchone()[0]
                data['streams'][rej[TAKER][STREAM]] = stream


        # Use trade details of maker iff no taker
        if not rej[TAKER]:
            who = MAKER
            lp = "null"
            stream = "null"
            linked_trade = "null"
            completed = False
        else:
            who = TAKER
            # A linked trade exists if it was part of a trade that
            # eventually completed
            linked_trade = rej[MAKER][TRADE_ID] \
                           if rej[MAKER][STATUS] == "C" else None
            completed = True

        if rej[who][TRADE_ID] not in data['rejections']:
            if rej[who][USD_RATE] != "": rate = float(rej[who][USD_RATE])
            else: rate = "null"

            q = "insert into rejections values (DEFAULT, {}, {}, {}, " +\
                "'{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, '{}', " +\
                "'{}', {}, '{}', '{}')"
            q = q.format(lp, stream, cp, account, rej[who][TRADE_ID],
                         rej[who][TRADE_DATE], rej[who][CURRENCY_PAIR],
                         rej[who][SPOTRATE], rej[who][BASE_AMT],
                         rej[who][TERM_AMT], rate, rej[who][USDAMT],
                         rej[who][COMMENTS], linked_trade, completed,
                         rej[who][TIME], rej[who][BUYSELL])
            curr.execute(q)

        count += 1

    conn.commit()

def push(raw_trades, raw_rejections, date):
    global session

    load_data()

    push_trades(raw_trades)
    push_rejections(raw_rejections)

    # If daily update, push rolls, costs and eod rates
    # if len(date.split("-")) == 3:
    #     costs.push(session)
    #     eod.push(date)
    #     rr.get()

    session.commit()