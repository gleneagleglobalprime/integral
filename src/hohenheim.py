import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, \
                       Float, DateTime, Text, BigInteger, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine
 
Base = declarative_base()

class CPs(Base):
    __tablename__ = 'cps'

    id   = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)

class LPs(Base):
    __tablename__ = 'lps'

    id   = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)

class Accounts(Base):
    __tablename__ = 'accounts'

    id  = Column(String(20), primary_key=True)
    cp  = Column(Integer, ForeignKey('cps.id'), nullable=False)
    cps = relationship(CPs)

class Streams(Base):
    __tablename__ = 'streams'

    id   = Column(String(20), primary_key=True)
    name = Column(String(20), nullable=False)
    lp   = Column(Integer, ForeignKey('lps.id'), nullable=False)
    lps  = relationship(LPs)

class Makers(Base):
    __tablename__ = 'makers'

    id = Column(Integer, primary_key=True)
    cp = Column(Integer, ForeignKey('cps.id'), nullable=False)
    buy_sell = Column(String(1), nullable=False)
    spot     = Column(Float, nullable=False)
    base_amt = Column(Float, nullable=False)
    term_amt = Column(Float, nullable=False)
    usd_rate = Column(Float)
    usd_amt  = Column(Float, nullable=False)
    trade_id = Column(String(20), nullable=False)
    profit   = Column(Float, nullable=False)
    account  = Column(String(20), ForeignKey('accounts.id'),
                             nullable=False)
    cps = relationship(CPs)
    accounts = relationship(Accounts)

class Trades(Base):
    __tablename__ = 'trades'

    id    = Column(Integer, primary_key=True)
    maker = Column(Integer, ForeignKey('makers.id'),
                            nullable=False)
    c_pair     = Column(String(7), nullable=False)
    trade_date = Column(DateTime, nullable=False)
    value_date = Column(DateTime, nullable=False)

    makers = relationship(Makers)

class Takers(Base):
    __tablename__ = 'takers'

    id = Column(Integer, primary_key=True)
    lp = Column(Integer, ForeignKey('lps.id'), nullable=False)
    buy_sell = Column(String(1), nullable=False)
    spot     = Column(Float, nullable=False)
    term_amt = Column(Float, nullable=False)
    usd_rate = Column(Float)
    usd_amt  = Column(Float, nullable=False)
    trade_id = Column(String(20), nullable=False)
    profit   = Column(Float, nullable=False)
    stream   = Column(String(20), ForeignKey('streams.id'),
                             nullable=False)
    trade    = Column(Integer, ForeignKey('trades.id'))

    lps = relationship(LPs)
    streams = relationship(Streams)
    trades = relationship(Trades,
                backref=backref('trades', uselist=True))

class Rejections(Base):
    __tablename__ = 'rejections'

    id = Column(Integer, primary_key=True)
    lp = Column(Integer, ForeignKey('lps.id'))
    cp = Column(Integer, ForeignKey('cps.id'), nullable=False)
    stream       = Column(String(20), ForeignKey('streams.id'))
    account      = Column(String(20), ForeignKey('accounts.id'),
                                      nullable=False)
    trade_id     = Column(String(20), nullable=False)
    trade_date   = Column(DateTime, nullable=False)
    c_pair       = Column(String(7), nullable=False)
    spot         = Column(Float, nullable=False)
    base_amt     = Column(Float, nullable=False)
    term_amt     = Column(Float, nullable=False)
    usd_rate     = Column(Float)
    usd_amt      = Column(Float, nullable=False)
    comments     = Column(Text)
    linked_trade = Column(String(20))
    completed    = Column(Boolean, nullable=False)

    lps = relationship(LPs)
    cps = relationship(CPs)
    streams  = relationship(Streams)
    accounts = relationship(Accounts)

class AggregationCosts(Base):
    __tablename__ = "aggregation_costs"

    id = Column(Integer, primary_key=True)
    lp = Column(Integer, ForeignKey('lps.id'), nullable=False)
    harmony_id = Column(BigInteger, nullable=False)
    trade_date = Column(DateTime, nullable=False)
    c_pair     = Column(String(7), nullable=False)
    cost       = Column(Float, nullable=False)

    lps = relationship(LPs)

engine = create_engine('postgresql://adrian:secret@localhost:5432/trades2')
