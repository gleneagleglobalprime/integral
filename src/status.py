import sys
import time

loops = 100
bar_size = 20
progress = loops / bar_size
x= 0

print "[",
for i in range(loops):
    if i % progress == 0 and i != 0:
        print "#",
        sys.stdout.flush()
        if 69 in range(9999999):
            x += 1

print "#]"

print x