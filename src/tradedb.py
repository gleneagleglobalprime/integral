#!/usr/bin/python
import alchemypush
import directories as dirs
import getopt
import glob
import match
import os
import rejections
import sys
import time
import utils.date as ud

#inn = dirs.root + "Yield Analysis/in/"
#err = dirs.root + "Yield Analysis/error/"
#out = dirs.root + "Yield Analysis/out/"
inn = dirs.root + "Yield Analysis\\in\\"
err = dirs.root + "Yield Analysis\\error\\"
out = dirs.root + "Yield Analysis\\out\\"

suffix = ".csv"

def main(argv):
    files = glob.glob(inn + "*")
    
    # Take first available file and inspect
    if files:
        for f in files:
            #f_name = f.split("/")[-1]
            f_name = f.split("\\")[-1]

            print "Looking at \"{}\"".format(f_name)
            if not f.endswith(suffix):
                # Move if not a .csv file
                print "{} is not a .csv, moving into error directory."\
                      .format(f_name)
                os.rename(f, err + f_name)

            else:
                date = ud.rename_date(f_name.split(".")[0].\
                                      replace("DoneTrades-GESA-", ""))
                # Begin processing
                # Match takers and makers and store in a
                # Trade class
                print "Matching up trades.."
                trades = match.out(f)
                print "Collecting rejections.."
                rejects = rejections.out(f)
                print "Commence pushing data to the db.."
                alchemypush.push(trades, rejects, date)

                # Finally, complete. Move files
                print "Done! Moving {} into out directory.".format(f_name)
                os.rename(f, out + f_name)


if __name__ == "__main__":
    main(sys.argv[1:])