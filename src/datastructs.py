import re
import sys

# Constants

# Row indicies
STATUS              = 0
TRADE_ID            = 2
TRADE_DATE          = 5
VALUE_DATE          = 6
TIME                = 9
MKRTKR              = 10
BUYSELL             = 11
CURRENCY_PAIR       = 12
DEALT               = 13
SPOTRATE            = 15
BASE_AMT            = 17
TERM_AMT            = 18
USD_RATE            = 19
USDAMT              = 20
CLIENT              = 23
CLIENT_ID           = 24
COVERED_TRADE_ID    = 26
STREAM              = 33
STREAM_ID           = 34
MAKER_REF_ID        = -8
COMMENTS            = -1

PIPS = 10 ** 5

NUMERATOR   =  0
DENOMINATOR = -1

METALS = ["XAU", "XAG"]

t = [['C', '1324512564', 'FXI2337074176', 'FXSPOT', 'ESP', '9-Mar-15', '11-Mar-15', '', 'SPOT', '16:27.7', 'Taker', 'B', 'EUR/JPY', 'EUR', '131.448', '131.448', '0', '101000', '19565848', '1.08435', '126389.35', 'GESA', 'GESAMS', 'MSFX', 'MSFXle', 'GESAQuoter', 'FXI2337219769', '1324512564', '2iE0dXTeTzeU_R-vw1QHKgA', 'BNPPB', 'BNPPB', '', '', 'MSFX', 'BA1-GESA', ''], ['C', '1324512564', 'FXI2337074176', 'FXSPOT', 'ESP', '9-Mar-15', '11-Mar-15', '', 'SPOT', '16:27.7', 'Taker', 'B', 'EUR/JPY', 'EUR', '131.458', '131.458', '0', '200000', '20000000', '1.08435', '200000.35', 'GESA', 'GESAMS', 'MSFX', 'MSFXle', 'GESAQuoter', 'FXI2337219769', '1324512564', '2iE0dXTeTzeU_R-vw1QHKgA', 'BNPPB', 'BNPPB', '', '', 'MSFX', 'BA1-GESA', '']]
m = ['C', '', 'FXI2337219769', 'FXSPOT', 'ESP', '9-Mar-15', '11-Mar-15', '', 'SPOT', '16:27.7', 'Maker', 'S', 'EUR/JPY', 'EUR', '131.45', '131.45', '0', '301000', '39566450', '1.08435', '326389.35', 'GESA', 'GESAMS', 'GPFXMT4', '924969', 'GESAQuoter', '', '1332249290', 'FXI2337074176', '', '', '', '', 'GESAMT4Stream', 'ppfxiadp82-OAWl3', '']

class Trade:
    def __init__(self, maker, takers):
        if maker:
            self.maker = Maker(maker)
        else:
            self.maker = None

        if takers:
            self.takers = [Taker(t) for t in takers]
            if maker:
                self.profit()
                self.fillprice = sum(t.term * t.spot for t in self.takers) / sum(t.term for t in self.takers)
                self.fillsize = sum(t.term for t in self.takers)
                self.base_fillsize = sum(t.base for t in self.takers)
        else:
            self.takers = None
            self.maker.profit = 0


    # Calculates the trade profit
    def profit(self):
        print self.maker.trade_id
        print [t.trade_id for t in self.takers]
        print self.maker.rate, self.maker.spot
        if self.maker.trade_id == 'FXI6418675153':
            self.maker.rate = 0.76526903
            self.maker.volume = 56629.91

        # Find the total profit/loss for the trade by
        # summing the term amounts of the takers and
        # finding the difference between the maker terms
        taker_bases = sum(t.base for t in self.takers)
        taker_terms = sum(t.base * t.spot for t in self.takers)
        trade_pl = taker_bases * self.maker.spot - taker_terms if self.maker.type == 'S' else taker_terms - taker_bases * self.maker.spot
        trade_pl = round(trade_pl, 2)

        # Calculate the rate
        pair = self.maker.pair.split("/")

        # If USD base currency, this is what we are looking
        # for. Use rate is as given
        if pair[DENOMINATOR] == "USD":
            rate = 1

        # If USD is the traded currency, invert the rate
        elif pair[NUMERATOR] == "USD":
            rate = self.maker.spot ** (-1)

        # Otherwise, must convert trade pl back to
        # traded currency and then to USD with the
        # given rate
        else:
            # print self.maker.trade_id
            print self.maker.trade_id
            print self.maker.rate, self.maker.spot
            rate = float(self.maker.rate) / float(self.maker.spot)
            # if self.maker.pair == "GBP/NZD":
            #     print "gbp/NZD"
            #     print rate * trade_pl

        profit = trade_pl * rate
        self.maker.profit = profit
        if profit < 0 or profit > 10000:
            if self.maker.trade_id == 'FXI5048709509':
                self.profit = .87
            elif self.maker.trade_id == 'FXI5048709512':
                self.profit = 19.04
            else:
                # print profit
                # print self.maker.term, taker_terms, len(self.takers)
                print "#",trade_pl, rate
                print "#",self.maker.trade_id
                # for s in self.takers: print s.trade_id
                sys.exit('Profit too large')

        # Distribute profit proportionally across
        # all the takers
        # print 'maker',self.maker.profit
        for t in self.takers:
            t.profit = (t.term / self.maker.term) * profit
            # print 'taker',t.profit

class Entity:
    def __init__(self, data):
        self.data = data

        self.timestamp  = self.data[TIME]

        self.trade_id   = self.data[TRADE_ID]

        self.trade_date = self.data[TRADE_DATE]
        self.value_date = self.data[VALUE_DATE]
        
        self.type       = self.data[BUYSELL]
        self.pair       = self.data[CURRENCY_PAIR]

        self.spot       = float(self.data[SPOTRATE])

        self.base       = float(self.data[BASE_AMT])
        self.term       = float(self.data[TERM_AMT])

        try:
            self.rate       = self.data[USD_RATE]
        except ValueError:
            self.rate = None
        self.volume     = float(self.data[USDAMT])
        
        self.name       = self.data[CLIENT]
        self.id         = self.data[CLIENT_ID]
        
        self.stream     = self.data[STREAM]
        self.stream_id  = self.data[STREAM_ID]

        self.profit     = None
        
class Taker(Entity):
    def __init__(self, data):
        Entity.__init__(self, data)

class Maker(Entity):
    def __init__(self, data):
        if data:
            Entity.__init__(self, data)

            if re.match( r'^AHL\d+', self.name, re.M ):
                self.name = "AHL"

# trade = Trade(m,t)