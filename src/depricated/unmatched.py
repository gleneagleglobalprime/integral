import directories as dirs
import xlwt

path = dirs.root + "Yield Analysis/unmatched/"

def out(f_name, takers, makers):
    book_name = f_name.split(".")[0]
    name  = book_name + "-UNMATCHED.xls"

    book  = xlwt.Workbook()
    sheet = book.add_sheet(name)

    row = 0
    for taker in takers:
        for col in range(len(taker)):
            sheet.write(row, col, taker[col])

        row += 1

    for maker in makers:
        for col in range(len(maker)):
            sheet.write(row, col, maker[col])

        row += 1

    book.save(path + name)