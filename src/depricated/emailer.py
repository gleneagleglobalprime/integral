from mailer import Mailer
from mailer import Message

message = Message(From="adrian.freedman@gleneagle.com.au",
                  To="adrian.freedman@gleneagle.com.au",
                  charset="utf-8")
message.Subject = "An HTML Email"
message.Html = """This email uses <strong>HTML</strong>!"""
message.Body = """This is alternate text."""

sender = Mailer('outlook.office365.com')
sender.send(message)