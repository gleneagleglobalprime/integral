import re

YEAR    = -1
MONTH   = -2
DAY     = -3

MONTH_LEN = 4
DAY_LEN   = 5

def rename_file(name):
    parts = name.split("-")

    day   = parts[DAY] + "-" if len(parts) == DAY_LEN else ""
    month = parts[MONTH]
    year  = parts[YEAR]

    return year + "/" + month + "/" + day + month + ".xls"

# rename_file("DoneTrades-GESA-Feb-2015")