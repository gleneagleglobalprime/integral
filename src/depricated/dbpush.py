#!/usr/bin/python

import psycopg2
import sys
from datastructs import *

MAKER = 0
TAKER = 1

conn = None
cur  = None

data = {}

def connect():
    global conn
    try:
        conn = psycopg2.connect("dbname='trades' user='adrian' host='localhost' password='secret'")
    except Exception, e:
        print "Unable to connect to the database!"
        print e

# Creates an entry into either the cp or lps table for the
# given name
def create_lcp(lcp, name):
    global cur

    q = "insert into {}ps values (default, '{}') " + \
        "returning id"
    q = q.format(lcp, name)
    cur.execute(q)
    res = cur.fetchall()

    return res[0][0]

def create_acc(acc, cp):
    global cur

    q = "insert into accounts values ('{}', {}) " + \
        "returning *"
    q = q.format(acc, cp)
    cur.execute(q)
    res = cur.fetchall()

    return res[0][0]

def create_mkr(mkr, cp_id, acc_id):
    global cur

    if not mkr.rate: mkr.rate = "null"

    q = "insert into makers values (default, {}, '{}', " + \
        "{}, {}, {}, {}, {}, '{}', {}, '{}') returning *"
    q = q.format(cp_id, mkr.type, mkr.spot, mkr.base, \
        mkr.term, mkr.rate, mkr.volume, mkr.trade_id, \
        mkr.profit, acc_id)
    cur.execute(q)
    res = cur.fetchall()

    return res[0][0]

def create_str(name, lp):
    global cur

    q = "insert into streams values (default, '{}', {})" + \
        " returning *"
    q = q.format(name, lp)
    cur.execute(q)
    res = cur.fetchall()

    return res[0][0]

def create_trade(cp, cp_id):
    global cur
    
    q = "insert into trades values (default, {}, '{}', " + \
        "'{}', '{}') returning *"
    q = q.format(cp_id, cp.pair, cp.trade_date, \
        cp.value_date)
    # print q
    cur.execute(q)
    res = cur.fetchall()

    return res[0][0]

def create_tkr(tkr, lp, stream, trade):
    global cur

    if not tkr.rate: tkr.rate = "null"
    profit = "{0:.4f}".format(round(tkr.profit, 4))

    q = "insert into takers values (default, {}, '{}', " + \
        "{}, {}, {}, {}, {}, {}, '{}') returning *"
    q = q.format(lp, tkr.type, tkr.spot, tkr.term, \
                 tkr.rate, tkr.volume, profit, stream, \
                 tkr.trade_id)
    cur.execute(q)
    res = cur.fetchall()

    taker_id = res[0][0]

    # Now, with the taker id, create the trade taker entry
    q = "insert into trade_takers values ({}, {}) " + \
        "returning *"
    q = q.format(trade, taker_id)
    cur.execute(q)

    return taker_id

# Fetches what's in the database to avoid double querying
# when inserting
def get_in_db():
    global cur, data

    q = "select name, id from cps"
    cur.execute(q)
    # Strip out whitespace and put in data
    data['cps'] = [tuple([r[0].strip(), r[1]]) \
                   for r in sorted(cur.fetchall())]

    data['accounts'] = []
    q = "select id from accounts"
    cur.execute(q)
    data['accounts'] = sorted(cur.fetchall())

    data['makers'] = []
    q = "select trade_id, id from makers"
    cur.execute(q)
    data['makers'] = sorted(cur.fetchall())

    data['trades'] = []
    q = "select m.trade_id, t.id from makers m join trades t" +\
        " on t.maker = m.id"
    cur.execute(q)
    data['trades'] = sorted(cur.fetchall())

    data['takers'] = []
    q = "select trade_id from takers"
    cur.execute(q)
    data['takers'] = sorted(cur.fetchall())

    data['lps'] = []
    q = "select name, id from lps order by name"
    cur.execute(q)
    data['lps'] = sorted(cur.fetchall())

    data['streams'] = []
    q = "select name, id from streams order by name"
    cur.execute(q)
    data['streams'] = sorted(cur.fetchall())

def push(trades_raw):
    global conn, cur, data

    # Put trades in correct datastructs
    trades = [Trade(t[MAKER], t[TAKER]) for t in trades_raw]

    connect()
    cur = conn.cursor()

    print "Loading data from db.."
    get_in_db()

    counter = 0

    for t in trades:
        # Fetch cp id. If cp name in db, fetch the id, otherwise
        # create a new entry in the db
        cp = t.maker
        if any(cp.name.strip() in c for c in data['cps']):
            cp_id = next(c for c in data['cps']
                         if c[0] == cp.name.strip())[1]
        else:
            cp_id = create_lcp("c", cp.name)

        # Fetch cp account id
        if any(str(cp.id) in a for a in data['accounts']):
            acc_id = cp.id
        else:
            acc_id = create_acc(cp.id, cp_id)

        # Fetch maker id
        if any(cp.trade_id in m for m in data['makers']):
            mkr_id = next(m for m in data['makers']
                          if m[0] == cp.trade_id)[1]
        else:
            mkr_id = create_mkr(cp, cp_id, acc_id)

        # Fetch trade id
        if any(cp.trade_id in t for t in data['trades']):
            trade = next(t for t in data['trades']
                         if t[0] == cp.trade_id)[1]
        else:
            trade = create_trade(cp, mkr_id)

        # Fetch lps ids
        lp_ids = []
        for lp in t.takers:
            if any(lp.name in l for l in data['lps']):
                lp_ids.append(next(l for l in data['lps']
                                   if l[0] == lp.name)[1])
            else:
                lp_ids.append(create_lcp("l", lp.name))

        # Fetch stream ids
        stream_ids = []
        i = 0
        for lp in t.takers:
            if any(lp.stream in s for s in data['streams']):
                stream_ids.append(next(s for s in
                    data['streams'] if s[0] == lp.stream)[1])
            else:
                stream_ids.append(create_str(lp.stream,
                                             lp_ids[i]))
            i += 1

        # Finish with takers
        i = 0
        for lp in t.takers:
            if any(lp.trade_id in t for t in data['takers']):
                pass
            else:
                create_tkr(lp, lp_ids[i], stream_ids[i], trade)

        if counter % round(len(trades)/width, 0) == 0:
            sys.stdout.write("%")
            sys.stdout.flush()
        counter += 1

        

        conn.commit()