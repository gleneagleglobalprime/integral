import timeit
import random

DATA_LEN = 500000
NUM_TESTS = 1000

d = {}
l = []

def load():
    global d,l

    for i in range(DATA_LEN):
        d[i] = i
        l.append(i)

def find_list():
    global l

    i = random.randint(0,DATA_LEN-1)
    next(x for x in l if x==i)

def find_dict_slow():
    global d

    i = random.randint(0,DATA_LEN-1)
    i in d.keys()

def find_dict_fast():
    global d

    i = random.randint(0,DATA_LEN-1)
    i in d


print "load data:", timeit.timeit(load, number=1)

random.shuffle(l)
print "search list:", timeit.timeit(find_list, number=NUM_TESTS)
print "search dict slow:", timeit.timeit(find_dict_slow, number=NUM_TESTS)
print "search dict fast:", timeit.timeit(find_dict_fast, number=NUM_TESTS)
