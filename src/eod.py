import json
import utils.connect
import requests

def get(date):
    app_id = "54b208c2cbe144529f23ed49ee13c299"
    url = "http://openexchangerates.org/api/historical/{}.json?app_id="
    url += app_id
    url = url.format(date)

    response = requests.get(url)
    return json.loads(response.text)['rates']

def push(date):
    conn, curr = utils.connect.psql()

    # Check that date is not already in the db
    q = "select id from eod_rates where date='{}'".format(date)
    curr.execute(q)

    if len(curr.fetchall()) != 0:
        print "EOD rates for {} already uploaded".format(date)

    else:
        eod = get(date)
        for code in eod:
            q = "insert into eod_rates values (DEFAULT, '{}', '{}', {})"
            q = q.format(date, code, eod[code])
            curr.execute(q)
        print "Successfully uploaded EOD rates:", date

    conn.commit()
    conn.close()


import datetime

today = datetime.datetime.today()
start = datetime.datetime.strptime('2015-09-04', "%Y-%m-%d")

while start < today:
    if start.weekday() <= 4:
        push(str(start).split(" ")[0])

    start += datetime.timedelta(1)
