from collections import defaultdict
from datastructs import *
import time

# Constants
MAKER   = 0
TAKER   = 1

PROFIT  = 0
VOLUME  = 1
TICKETS = 2

# lps and cps are default dict of type list which
# store total profits and volume traded for the l/cp
lps = defaultdict(list)
cps = defaultdict(list)

# steams is a dictionary which has a collections of
# default dicts in the exact same manner as the l/cp
# ones. They are stored in this manner as a method of
# filtration. E.g. the "MST" entry has only profits
# and volumes for trades executed through an MST client
streams = {
            "all":     defaultdict(list),
            "GPFXMT4": defaultdict(list),
            "MST":     defaultdict(list),
            "YIB":     defaultdict(list),
            "ALS":     defaultdict(list),
            "AHL":     defaultdict(list)
          }

# currencies is a dictionary which holds the the 
# stream profits and volumes for each currency pair
# for each organisation
# It maps a currency pair to a list of tuples of
# stream and stream IDs which maps to that streams
# volume and profit
currencies = {
               "all":     {},
               "GPFXMT4": {},
               "MST":     {},
               "YIB":     {},
               "ALS":     {},
               "AHL":     {}
             }

volumes = {
            "all":     {},
            "GPFXMT4": {},
            "MST":     {},
            "YIB":     {},
            "ALS":     {},
            "AHL":     {}
          }

# Sums the total volume for each currency pair
# DEPRICATED
def calculate_total_currency_volumes():
    total = {
              "all":     {},
              "GPFXMT4": {},
              "MST":     {},
              "YIB":     {}
            }

    for pair, vols in currencies.iteritems():
        total["all"][pair] = sum(v[1][VOLUME] for v in vols.items())
        # total["GPFXMT4"][pair] = sum(v[1][VOLUME] for v in vols.items() if )
        total["all"][pair] = sum(v[1][VOLUME] for v in vols.items())
        total["all"][pair] = sum(v[1][VOLUME] for v in vols.items())

    return total


# Calculates the total volume of each currency traded by
# summing the different LP streams
def calculate_currency_volumes(trade):
    # See if pair exists in the dictionary, if not, create it
    pair = trade.maker.pair
    if pair not in currencies["all"]:
        currencies["all"][pair] = {}

    # Do the same for the org, if relevant
    org = trade.maker.name
    if org in currencies.keys():
        if pair not in currencies[org]:
            currencies[org][pair] = {}

    for t in trade.takers:
        # Create a tuple of LP and stream for an identifier
        lp_stream = (t.name, t.stream)

        # See if lp_stream exists for this pair
        if lp_stream not in currencies["all"][pair]:
            currencies["all"][pair][lp_stream] = [0, 0, 0]
        currencies["all"][pair][lp_stream][PROFIT] += t.profit
        currencies["all"][pair][lp_stream][VOLUME] += t.volume
        currencies["all"][pair][lp_stream][TICKETS] += 1

        # See if lp_stream exists for this orgs pair
        if org in currencies.keys():
            # print org, pair, lp_stream
            if lp_stream not in currencies[org][pair]:
                currencies[org][pair][lp_stream] = [0, 0, 0]
                # print lp_stream, org, pair
            currencies[org][pair][lp_stream][PROFIT] += t.profit
            currencies[org][pair][lp_stream][VOLUME] += t.volume
            currencies[org][pair][lp_stream][TICKETS] += 1
            # print currencies[org][pair][lp_stream][PROFIT],currencies[org][pair][lp_stream][VOLUME]

        # Add to total volumes
        if pair not in volumes["all"]:
            volumes["all"][pair] = t.volume
        else:
            volumes["all"][pair] += t.volume

        # Add orgs volumes
        if org in volumes.keys():
            if pair not in volumes[org]:
                volumes[org][pair] = t.volume
            else:
                volumes[org][pair] += t.volume

def out(trades):
    # Calculate cumulative totals for takers and makers
    for trade in trades:
        t = Trade(trade[MAKER], trade[TAKER])

        # Add takers
        for taker in t.takers:
            # See if taker exists in dictionary. If not, create a new entry, else, update
            if taker.name not in lps.keys():
                lps[taker.name] = [taker.profit, taker.volume, 1]
            else:
                lps[taker.name][PROFIT]  += taker.profit
                lps[taker.name][VOLUME]  += taker.volume
                lps[taker.name][TICKETS] += 1

            # Do the same for streams
            for k, v in streams.iteritems():
                if k == "all" or \
                  (k != "all" and t.maker.name == k):
                    if taker.stream not in v.keys():
                        v[taker.stream] = [taker.profit, taker.volume, 1]
                    else:
                        v[taker.stream][PROFIT] += taker.profit
                        v[taker.stream][VOLUME] += taker.volume          
                        v[taker.stream][TICKETS] += 1

        # Add maker
        if t.maker.name not in cps.keys():
            cps[t.maker.id] = [t.maker.profit, t.maker.volume, 1]
        else:
            cps[t.maker.id][PROFIT]  += t.maker.profit
            cps[t.maker.id][VOLUME]  += t.maker.volume
            cps[t.maker.id][TICKETS] += 1

        # if t.maker.profit > 1000:
        #     print t.maker.profit, t.maker.data[MAKER_REF_ID]
        #     print trade[MAKER]
        #     print trade[TAKER]

        calculate_currency_volumes(t)

    #total_volumes = calculate_total_currency_volumes()

    return (lps, cps, streams, currencies, volumes)
