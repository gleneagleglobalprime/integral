#!/usr/bin/python

import calendar
import datetime
import directories as dirs
import os
import sys
import getopt
import utils.connect
import xlsxwriter
import xlwt

PROFIT_DIV = 1000000

YEAR    = 0
MONTH   = 1
DAY     = 2

FLAT_COSTS = 250000
REJECTION_TOLLERANCE = 10

MONTH_LEN = 4
DAY_LEN   = 5

CHAR_PIX  = 256

LP_NAME = 0
PROFIT  = 1
VOLUME  = 2
TICKETS = 3

ALL = 0
AHL = 1
ALS = 2
MT4 = 3
MST = 4
YIB = 5

orgs = []

path = dirs.root + "Yield Analysis/stats/"

formats = {}

title = xlwt.easyxf( 'font: bold 1; align: wrap 1;' )
currency_fmt = xlwt.XFStyle()
currency_fmt.num_format_str = "#,##0.00"
comma_fmt = xlwt.easyxf("", "#,###")
percent_fmt = xlwt.easyxf(num_format_str='0.00')
pct_pos_fmt = xlwt.easyxf('font: colour green;', num_format_str='0.00')
pct_neg_fmt = xlwt.easyxf('font: colour red;',   num_format_str='0.00')

def add_styles(book):
    formats["pct"] = book.add_format({"num_format": "0.00"})
    formats["cur"] = book.add_format({"num_format": "#,##0.00"})
    formats["com"] = book.add_format({"num_format": "#,###"})
    formats["pct_pos"] = book.add_format({"num_format": "0.00",
                                          "font_color": "green"})
    formats["pct_neg"] = book.add_format({"num_format": "0.00",
                                          "font_color": "red"})

def adjust_col_widths(sheet, cols):
    for c in range(cols):
        sheet.col(c).width = CHAR_PIX * 13

    return sheet

def create_lp_sheet(book, cur, start, end):
    sheet = book.add_worksheet("LPs")

    row = 0
    sheet.write(row, LP_NAME, "Name")
    sheet.write(row, PROFIT, "Yield")
    sheet.write(row, VOLUME, "Volume")
    sheet.write(row, TICKETS, "Tickets")
    sheet.write(row, 4, "Average Ticket Size")
    sheet.write(row, 5, "Rejections")
    sheet.write(row, 6, "Rejection %")
    sheet.write(row, 7, "Rejection Sums")
    sheet.write(row, 8, "Average Rejection Size")

    sheet.set_column(0, 8, 11.5)

    q = "select l.*, sum(r.rej), sum(r.rej_sum) from " +\
        "lp_yields('{}', '{}') l left outer join " +\
        "rejections('{}', '{}') r on l.name=r.lp group by l.name, " +\
        "l.profit, l.volume, l.tickets order by name"
    q = q.format(start, end, start, end)
    cur.execute(q)

    row += 1
    for r in cur.fetchall():
        sheet.write(row, LP_NAME, r[LP_NAME])
        sheet.write(row, PROFIT, r[PROFIT]/(r[VOLUME]/PROFIT_DIV),
                    formats["cur"])
        sheet.write(row, VOLUME,  r[VOLUME],  formats["com"])
        sheet.write(row, TICKETS, r[TICKETS], formats["com"])
        avg_ticket = r[VOLUME]/r[TICKETS] if r[TICKETS] > 0 else 0
        sheet.write(row, 4, avg_ticket, formats["com"])
        if r[4]:
            pct = (float(r[4])/float(r[3]+r[4]))*100
            style = formats["pct"] if pct <= REJECTION_TOLLERANCE \
                                 else formats["pct_neg"]

            sheet.write(row, 5, r[4], formats["com"])
            sheet.write(row, 6, pct,  style)
            sheet.write(row, 7, r[5], formats["com"])
            sheet.write(row, 8, float(r[5])/float(r[4]),
                                      formats["com"])
        row += 1

    sheet.autofilter(0, 0, row - 1, 8)

def create_currency_volume_sheet(book, cur, start, end):
    sheet = book.add_worksheet("Total Currency Volumes")

    row = 0
    sheet.write(row, 0, "Org")
    sheet.write(row, 1, "Rank")
    sheet.write(row, 2, "Currency")
    sheet.write(row, 3, "Volume")
    sheet.write(row, 4, "Yield")
    row += 1

    sheet.set_column(0, 4, 11.5)

    q = "select * from currency_volumes('{}', '{}', '{}%')"

    i = 0
    for org in orgs:
        o = "" if org == "all" else org

        q1 = q.format(start, end, o)
        cur.execute(q1)

        rank = 1
        for r in cur.fetchall():
            sheet.write(row, 0, org.upper())
            sheet.write(row, 1, rank)
            sheet.write(row, 2, r[0])
            sheet.write(row, 3, r[1], formats["com"])
            sheet.write(row, 4, r[2]/(r[1]/PROFIT_DIV),
                                      formats["cur"])
            rank += 1
            row += 1

        i += 1

    sheet.autofilter(0, 0, row - 1, 4)

def create_rank_sheet(book, cur, start, end):
    sheet = book.add_worksheet("Rankings")

    row = 0
    sheet.write(row, 0, "Org")
    sheet.write(row, 1, "Rank")
    sheet.write(row, 2, "Stream")
    sheet.write(row, 3, "Volume")
    sheet.write(row, 4, "Yield")
    row += 1

    sheet.set_column(0, 4, 11.5)

    q = "select rank() over (order by sum(volume) desc, " +\
        "sum(profit) desc), stream, sum(volume), sum(profit) " +\
        "from stream_org_rank where date>='{}' and date<='{}' and " +\
        "org like '{}%' group by stream order by sum(volume) desc, " +\
        "sum(profit) desc"

    i = 0
    for org in orgs:
        o = "" if org == "all" else org

        q1 = q.format(start, end, o)
        cur.execute(q1)

        for r in cur.fetchall():
            sheet.write(row, 0, org.upper())
            sheet.write(row, 1, r[0])
            sheet.write(row, 2, r[1])
            sheet.write(row, 3, r[2], formats["com"])
            sheet.write(row, 4, r[3]/(r[2]/PROFIT_DIV),
                                      formats["cur"])
            row += 1

        i += 1

    sheet.autofilter(0, 0, row - 1, 4)

def create_currency_rank_sheet(book, cur, start, end):
    sheet = book.add_worksheet("Currency Rankings")

    row = 0
    sheet.write(row, 0, "Org")
    sheet.write(row, 1, "Currency")
    sheet.write(row, 2, "Rank")
    sheet.write(row, 3, "Stream")
    sheet.write(row, 4, "Volume")
    sheet.write(row, 5, "Yield")
    row += 1

    sheet.set_column(0, 5, 11.5)
    
    q = "select c_pair, stream, sum(volume), sum(profit) from " +\
        "stream_org_curr_rank where date>='{}' and date<='{}' and org " +\
        "like '{}%' group by c_pair, stream order by c_pair, " +\
        "sum(volume) desc, sum(profit) desc"

    i = 0
    for org in orgs:
        o = "" if org == "all" else org

        q1 = q.format(start, end, o)
        cur.execute(q1)

        c_pair = ""
        for r in cur.fetchall():
            if c_pair != r[0]:
                rank = 1
                c_pair = r[0]
            else:
                rank += 1
            sheet.write(row, 0, org.upper())
            sheet.write(row, 1, r[0])
            sheet.write(row, 2, rank)
            sheet.write(row, 3, r[1])
            sheet.write(row, 4, r[2], formats["com"])
            sheet.write(row, 5, r[3]/(r[2]/PROFIT_DIV),
                                      formats["cur"])
            row += 1

        i += 1

    sheet.autofilter(0, 0, row - 1, 5)

def create_cp_sheet(book, cur, start, end):
    sheet = book.add_worksheet("Counter Parties")

    row = 0
    sheet.write(row, 0, "ID")
    sheet.write(row, PROFIT, "Yield")
    sheet.write(row, VOLUME, "Volume")
    sheet.write(row, TICKETS, "Tickets")
    sheet.write(row, 4, "Average Ticket Size")
    sheet.write(row, 5, "Rejections")
    sheet.write(row, 6, "Rejection %")
    sheet.write(row, 7, "Rejection Sums")
    sheet.write(row, 8, "Average Rejection Size")

    sheet.set_column(0, 8, 11.5)
    
    q = "select c.*, sum(r.rej), sum(r.rej_sum) from " +\
        "cp_yields('{}', '{}') c left outer join " +\
        "rejections_cps('{}', '{}') r on c.account=r.account " +\
        "group by c.cp, c.account, c.profit, c.volume, c.tickets " +\
        "order by c.account, c.cp"
    q = q.format(start, end, start, end)
    cur.execute(q)

    row += 1
    for r in cur.fetchall():
        if r[0] in ("AHL", "GPFXMT4"):
            account = r[0]
        else:
            account = r[1]
        sheet.write(row, 0, account)
        sheet.write(row, 1, r[2]/(r[3]/PROFIT_DIV),
                                  formats["cur"])
        sheet.write(row, 2, r[3], formats["com"])
        sheet.write(row, 3, r[4], formats["com"])
        avg_ticket = r[3]/r[4] if r[4] > 0 else 0
        sheet.write(row, 4, avg_ticket, formats["com"])
        if r[5]:
            pct = (float(r[5])/float(r[4]+r[5]))*100
            style = formats["pct"] if pct <= REJECTION_TOLLERANCE \
                                   else formats["pct_neg"]

            sheet.write(row, 5, r[5], formats["com"])
            sheet.write(row, 6, pct,  style)
            sheet.write(row, 7, r[6], formats["com"])
            sheet.write(row, 8, r[6]/float(r[5]), formats["com"])
        row += 1

    sheet.autofilter(0, 0, row - 1, 8)

def create_stream_sheets(book, cur, start, end):
    for org in orgs:
        if org == "all":
            q = "select o.*, sum(r.rej), sum(r.rej_sum) from " +\
                "org_yields('{}', '{}', 'all') o join " +\
                "rejections('{}', '{}') r on " +\
                "r.stream=o.name group by o.name, o.profit, o.volume, " +\
                "o.tickets order by o.name"
            q = q.format(start, end, start, end)
        else:
            q = "select o.*, r.rej, r.rej_sum, r.account, " +\
                "r.stream from " +\
                "org_yields('{}','{}', '{}') o " +\
                "full outer join " +\
                "rej_stream_org('{}', '{}', '{}') " +\
                "r on o.name=r.stream"
            q = q.format(start, end, org, start, end, org)

        cur.execute(q)
        res = cur.fetchall()

        if len(res) > 0:
            create_stream_sheet(book, res, org)

def create_stream_sheet(book, res, org):
    global currency_fmt, comma_fmt, pct_neg_fmt

    name = "" if org == 'all' else " | " + org
    sheet = book.add_worksheet("Streams" + name)

    row = 0
    sheet.write(row, 0, "Name")
    sheet.write(row, PROFIT, "Yield")
    sheet.write(row, VOLUME, "Volume")
    sheet.write(row, TICKETS, "Tickets")
    sheet.write(row, 4, "Average Ticket Size")
    sheet.write(row, 5, "Rejections")
    sheet.write(row, 6, "Rejection %")
    sheet.write(row, 7, "Rejection Sums")
    sheet.write(row, 8, "Average Rejection Size")
    sheet.write(row, 9, "Account")

    sheet.set_column(0, 9, 11.5)
    
    row += 1
    for r in res:
        # print r
        if r[0]:
            sheet.write(row, LP_NAME, r[LP_NAME])
            sheet.write(row, PROFIT, r[PROFIT]/(r[VOLUME]/PROFIT_DIV),
                        formats["cur"])
            sheet.write(row, VOLUME, r[VOLUME], formats["com"])
            sheet.write(row, TICKETS, r[TICKETS], formats["com"])
            avg_ticket = r[VOLUME]/r[TICKETS] if r[TICKETS] > 0 else 0
            sheet.write(row, 4, avg_ticket, formats["com"])
        elif r[7]:
            sheet.write(row, 0, r[7])

        if r[4]:
            pct = (float(r[4])/float(r[3]+r[4]))*100 \
                  if r[3] else None
            style = formats["pct"] if pct <= REJECTION_TOLLERANCE \
                                 else formats["pct_neg"]

            sheet.write(row, 5, r[4], formats["com"])
            sheet.write(row, 6, pct, style)
            sheet.write(row, 7, r[5], formats["com"])
            sheet.write(row, 8, r[5]/float(r[4]), formats["com"])
        if org != "all" and r[6]:
            sheet.write(row, 9, r[6])

        row += 1

    sheet.autofilter(0, 0, row - 1, 9)

def create_currency_sheets(book, cur, start, end, opt):
    for org in orgs:
        if org == "all":
            q = "select * from curr_vol_rej_costs('{}', '{}')"
            q = q.format(start, end, start, end)
        else:
            q = "select * from curr_vol_rej_orgs('{}','{}','{}')".\
                format(start, end, org)

        cur.execute(q)
        res = cur.fetchall()

        if len(res) == 0: continue

        if opt in ("-m", "--month", "-d", "--day"):
            start_dt = datetime.datetime.strptime(start, "%Y-%m-%d").date()

            if opt in ("-m", "--month"):
                last_l = start_dt - datetime.timedelta(days=1)
                last_s = datetime.date(day=1, month=last_l.month, year=last_l.year)
            elif opt in ("-d", "--day"):
                last_s = datetime.date(day=1, month=start_dt.month,
                                       year=start_dt.year)
                last_l = end

            q = "select * from stream_currencies('{}', '{}', '{}')".\
                format(last_s, last_l, org)
            cur.execute(q)
            res2 = cur.fetchall()

            q = "select c_pair, name, sum(volume) from lp_vol " +\
                "where date>='{}' and date <='{}'" +\
                "group by c_pair, name"
            q = q.format(start, end)
            cur.execute(q)
            lp_vol = cur.fetchall()

            print "create currency sheet...",
            create_currency_sheet(book, res, res2, lp_vol, org)
            print "done"

        else:
            create_currency_sheet(book, res, org)

def create_currency_sheet(book, res, org):
    name = "" if org == 'all' else " | " + org
    sheet = book.add_worksheet("Currency Volumes" + name)

    row = 0
    sheet.write(row, 0, "Currency Pairs")
    sheet.write(row, 1, "Currency Volume")
    sheet.write(row, 2, "LP")
    sheet.write(row, 3, "Stream")
    sheet.write(row, 4, "Stream Yield")
    sheet.write(row, 5, "Stream Volume")
    sheet.write(row, 6, "Volume %")
    sheet.write(row, 7, "Tickets")
    sheet.write(row, 8, "Average Ticket Size")

    sheet.set_column(0, 8, 11.5)

    if org == "all":
        sheet.write(row, 9, "Aggregation Cost")

    row += 1
    for r in res:
        sheet.write(row, 0, r[0])
        sheet.write(row, 1, r[1], formats["com"])
        sheet.write(row, 2, r[2])
        sheet.write(row, 3, r[3])
        sheet.write(row, 4, r[4]/(r[5]/PROFIT_DIV),
                                  formats["cur"])
        sheet.write(row, 5, r[5], formats["com"])
        sheet.write(row, 6, r[5]/r[1]*100, formats["pct"])
        sheet.write(row, 7, r[6], formats["com"])
        sheet.write(row, 8, r[5]/r[6], formats["com"])
        if org == "all":
            sheet.write(row, 9, r[7], formats["cur"])
        row += 1

    if org != "all":
        sheet.autofilter(0, 0, row - 1, 8)
    else:
        sheet.autofilter(0, 0, row - 1, 9)

def create_currency_sheet(book, res, res2, lp_vol, org):
    name = "" if org == 'all' else " | " + org
    sheet = book.add_worksheet("Currency Volumes" + name)

    row = 0
    sheet.write(row, 0, "Currency Pairs")
    sheet.write(row, 1, "Currency Volume")
    sheet.write(row, 2, "LP")
    sheet.write(row, 3, "Stream")
    sheet.write(row, 4, "Stream Yield")
    sheet.write(row, 5, "Stream Volume")
    sheet.write(row, 6, "Volume %")
    sheet.write(row, 7, "Last Volume %")
    sheet.write(row, 8, "Change")
    sheet.write(row, 9, "Tickets")
    sheet.write(row, 10, "Average Ticket")
    sheet.write(row, 11, "Rejections")
    sheet.write(row, 12, "Rejection %")
    sheet.write(row, 13, "Rejection Sums")
    sheet.write(row, 14, "Average Rejection")
    if org == "all":
        sheet.write(row, 15, "Aggregation Cost")
        sheet.write(row, 16, "Cost/Volume")
        sheet.set_column(0, 16, 11.5)
    else:
        sheet.set_column(0, 14, 11.5)

    row += 1
    # print org
    for r in res:
        # print row, r
        sheet.write(row, 0, r[0])
        sheet.write(row, 1, r[1], formats["com"])
        sheet.write(row, 2, r[2])
        sheet.write(row, 3, r[3])
        if r[4]:
            sheet.write(row, 4, r[4]/(r[5]/PROFIT_DIV), formats["cur"])
            sheet.write(row, 5, r[5], formats["com"])
            pct = r[5]/r[1]*100
            sheet.write(row, 6, pct, formats["pct"])

            try:
                last = next(r2 for r2 in res2 if r2[0] == r[0] and r2[3] == r[3])

                last_pct = last[5]/last[1]*100
                delta = pct - last_pct
                if delta > 0:
                    delta_fmt = formats["pct_pos"]
                elif delta == 0:
                    delta_fmt = formats["pct"]
                elif delta < 0:
                    delta_fmt = formats["pct_neg"]
                sheet.write(row, 7, last_pct, formats["pct"])
                sheet.write(row, 8, delta, delta_fmt)

            except StopIteration:
                pass

            sheet.write(row, 9, r[6], formats["com"])
            sheet.write(row, 10, r[5]/float(r[6]), formats["com"])
            
            if r[7]:
                pct = (float(r[7])/float(r[6]+r[7]))*100
                style = formats["pct"] if pct <= REJECTION_TOLLERANCE \
                                       else formats["pct_neg"]

                sheet.write(row, 11, r[7], formats["com"])
                sheet.write(row, 12, pct, style)
                sheet.write(row, 13, r[8], formats["com"])
                sheet.write(row, 14, r[8]/float(r[7]), formats["com"])

        if org == "all":
            cost = r[9] if r[9] else r[5] / FLAT_COSTS
            sheet.write(row, 15, cost, formats["cur"])

            try:
                lv = next(l for l in lp_vol if l[0] == r[0] and l[1] == r[2])
            except StopIteration:
                pass

            cost_pm = (cost / (lv[2] / 1000000)) if r[9] else 4
            sheet.write(row, 16, cost_pm, formats["cur"])
        row += 1

    if org != "all":
        sheet.autofilter(0, 0, row - 1, 14)
    else:
        sheet.autofilter(0, 0, row - 1, 16)

def rename(date_str):
    try:
        date = datetime.datetime.strptime(date_str, "%Y-%m-%d")
        return datetime.datetime.strftime(date, "%Y/%m-%b/%d.xlsx")
    except ValueError:
        date = datetime.datetime.strptime(date_str, "%Y-%m")
        return datetime.datetime.strftime(date, "%Y/%m-%b/%b.xlsx")

def usage():
    print "usage: yields must use the flags [h,d,w,m]"
    print "yields -h: displays the usage summary"
    print "yields -d <date>: generate report for the given date"
    print "yields -w <start date> <end date>: " +\
          "generate report for the given date range"
    print "yields -m <date>: generate report for the given month"

def vali_date(date):
    try:
        datetime.datetime.strptime(date, '%Y-%m-%d')
    except ValueError:
        print "yields: invalid date! date should be of the format YYYY-MM-DD"
        sys.exit(2)

def vali_month(date):
    try:
        datetime.datetime.strptime(date, '%Y-%m')
    except ValueError:
        print "yields: invalid date! date should be of the format YYYY-MM"
        sys.exit(2)

def main(argv):
    # Check arguments
    if len(argv) < 2:
        usage()
        sys.exit(2)

    try:
        opts, args = getopt.getopt(argv, "hd:w:m:",
                            ["help", "day=", "week", "month="])
    except getopt.GetOptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-d", "--day"):
            vali_date(arg)
            start = arg
            end = arg
        elif opt in ("-w", "--week"):
            vali_date(argv[1])
            vali_date(argv[2])
            start = argv[1]
            end = argv[2]
        elif opt in ("-m", "--month"):
            vali_month(arg)
            start = arg + "-01"
            end = arg + "-" + \
                  str(calendar.monthrange(int(arg[:4]), int(arg[6:8]))[1])

    conn, curr = utils.connect.psql()
    cur  = conn.cursor()

    # Identify all counter parties
    q = "select distinct c.name from cps c join makers m on " +\
        "c.id=m.cp join trades t on t.maker=m.id where " +\
        "t.trade_date>='{}' and t.trade_date<='{}' order by c.name"
    q = q.format(start, end)

    cur.execute(q)
    res = cur.fetchall()

    global orgs
    orgs = ["all"] + [r[0].strip() for r in res]

    name = rename(start[:-3]) if opt in ("-m", "--month") \
                              else rename(start)
    book = xlsxwriter.Workbook(path + name)

    add_styles(book)
    
    print "lp sheet..",
    create_lp_sheet(book, cur, start, end)
    print "done"
    print "currency sheet...",
    create_currency_volume_sheet(book, cur, start, end)
    print "done"
    print "rank..",
    create_rank_sheet(book, cur, start, end)
    print "done"
    print "currency_rank,..",
    create_currency_rank_sheet(book, cur, start, end)
    print "done"
    create_currency_sheets(book, cur, start, end, opt)
    create_stream_sheets(book, cur, start, end)
    create_cp_sheet(book, cur, start, end)
    book.close()
    sys.exit()

    book = xlwt.Workbook()



    if opt in ("-m", "--month"):
        book.save(path + rename(start[:-3]))
    else:
        book.save(path + rename(start))



if __name__ == "__main__":
    main(sys.argv[1:])