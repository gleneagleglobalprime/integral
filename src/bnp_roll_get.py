#import csv
#import datetime
#import directories as dirs
#import utils.connect
#import utils.date as ud

#TRADE_DATE  = 0
#NEUMERATOR  = 3
#LONG        = 4
#DENOMINATOR = 5
#SHORT       = 6
#VALUE_DATE  = 7
#MTM         = 8

#B_PAIR  = 2
#B_POS   = 3
#B_EXTRA = 5

#M_PAIR = 0
#M_POS  = 1

#path = dirs.share
#f = path + "/NetPositionsByCurrencyPair.csv"

#bnp = []

#with open(f, "rb") as infile:
#    reader = csv.reader(infile)
#    # Skip the header
#    next(reader, None)

#    rows = [row for row in reader]

#bnp = []

#for r in rows:
#    trade_date = datetime.datetime.strptime(r[TRADE_DATE], "%d-%b-%Y")
#    value_date = datetime.datetime.strptime(r[VALUE_DATE], "%d-%b-%Y")
#    pair = r[NEUMERATOR] + r[DENOMINATOR]
#    position = float(r[LONG])
#    mtm = float(r[MTM])

#    # Value date is T+2 unless currency is USD/CAD or USD/TRY
#    day_diff = 1 if pair in ("USDCAD", "USDTRY") else 2
#    if (value_date - trade_date).days == day_diff:
#        bnp.append([trade_date, value_date, pair, position, mtm])

#    # Sometimes position not finished until day after settlement
#    elif (value_date - trade_date).days > day_diff and pair != "USDCAD":
#        try:
#            index = bnp.index(next(b for b in bnp if b[B_PAIR] == pair))
#            bnp[index].append(position)
#        except StopIteration:
#            print "No position matching {} from {}".format(pair,
#                datetime.datetime.strftime(trade_date, "%Y-%m-%d"))

## Adding day as position must be the EOD positions
#date = datetime.datetime.strftime(bnp[0][0] + datetime.timedelta(1),
#                                  "%Y-%m-%d 00:00:00")

#curr = utils.connect.mysql()
#q = "SELECT SYMBOL, SUM(-volume*2*(CMD-0.5))*1000 FROM MT4_TRADES t " +\
#    "JOIN MT4_USERS u ON t.LOGIN=u.LOGIN WHERE t.CMD IN (0,1) and " +\
#    "((CLOSE_TIME='1970-01-01' AND OPEN_TIME<'{}') OR " +\
#    "(CLOSE_TIME>='{}' AND OPEN_TIME<'{}')) AND NOT u.GROUP like 'GPMS%' "+\
#    "GROUP BY SYMBOL ORDER BY SYMBOL"
#q = q.format(date, date, date)

#curr.execute(q)
#mt4 = curr.fetchall()

#for m in mt4:
#    # Metals have different 
#    if m[M_PAIR] == "XAGUSD":
#        m = (m[M_PAIR], m[M_POS] / 20)
#    elif m[M_PAIR] == "XAUUSD":
#        m = (m[M_PAIR], m[M_POS] / 1000)

#    out = "{}: {}, ".format(m[M_PAIR], m[M_POS])
#    try:
#        b_rec = next(bb for bb in bnp if bb[B_PAIR] == m[M_PAIR])
#    except StopIteration:
#        continue

#    b = b_rec[B_POS]
#    extra = b_rec[B_EXTRA] if len(b_rec) == B_EXTRA + 1 else 0

#    if b == m[M_POS]:
#        out += str(b)
#    elif b + extra == m[M_POS]:
#        out +=  str(b + extra)
#    else:
#        out += str(b)
#        out += " EXPOSURE OF {}".format(float(m[M_POS]) - b)
#        out += " ({})".format(float(m[M_POS]) - (b + extra)) if extra else ""

#    print out
