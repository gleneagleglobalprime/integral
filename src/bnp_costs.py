import csv
import datetime
import os
import shutil
from utils import connect, pgp
import zipfile

PATH = '/home/adrian/share/bnp_costs/'
PGP  = PATH + 'pgp/'
ZIP  = PATH + 'zip/'

START = datetime.datetime(2016, 3, 1)
END = datetime.datetime(2016, 4, 1)
FTP = connect.bnp_ftp()
GPG = pgp.init()

today = END
costs = [['Activity Date', 'Activity', 'BNP Acct', 'TD', 'Symbol', 'Rate', 'Term', 'VD', 'Ticket', 'LP', 'Source', 'Customer Trade ID', 'Timestamp (GMT)']]

while today <= END:
    # Skip weekends
    if today.weekday() < 5:
        today_str = today.strftime("%Y-%m-%d")
        print today_str

        # Get file
        from_ = "./FromBNPP/Statement_GLENEAGLE_SECURITIES__AUST__PTY_LIMITED_{}.zip.pgp".format(today.strftime("%Y_%m_%d"))
        to = "{0}{1}.zip.pgp".format(PGP, today.strftime("%Y-%m-%d"))
        FTP.get(from_, to)

        # Decrypt
        zip_path = ZIP + today_str + '.zip'
        with open(to, 'rb') as f:
            status = GPG.decrypt_file(f, passphrase='Gleneagle1', output=zip_path)
            if not status.ok: print today_str, 'unable to decrypt!'

        # Decompress
        with zipfile.ZipFile(zip_path) as z:
            source = z.open('Fx_Activity.csv')
            cdir = os.path.join(PATH, today_str + '.csv')
            target = file(cdir, 'wb')
            with source, target:
                shutil.copyfileobj(source, target)

        cf = csv.reader(open(cdir, 'rU'))
        daily = [c for c in cf if c[0] != 'Activity Date']

        costs += [[d[0], d[1], d[2], d[3], d[4] + d[7], d[5], d[6], d[8], d[9], d[10], d[12], d[13], d[15], d[18]] for d in daily]
        
    today += datetime.timedelta(days=1)

# with open(PATH + 'total.csv', 'wb') as f:
#     writer = csv.writer(f)
#     for c in costs: writer.writerow(c)
