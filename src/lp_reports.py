#!/usr/bin/python

import calendar
import datetime
import directories as dirs
import os
import string
import sys
import getopt
import utils.connect
import xlsxwriter
import xlwt

PROFIT_DIV = 1000000

YEAR    = 0
MONTH   = 1
DAY     = 2

FLAT_COSTS = 250000
REJECTION_TOLLERANCE = 15

MONTH_LEN = 4
DAY_LEN   = 5

CHAR_PIX  = 256

LP_NAME     = 0
LP_STREAM   = 1
C_PAIR      = 2
C_VOL       = 3
STREAM_VOL  = 4
PERCENT     = 5
S_TICKETS   = 6
AVG_TICKET  = 7

orgs = ('all', 'AHL', 'ALS', 'GPFXMT4', 'MST', 'YIB')

lps = []

title = xlwt.easyxf( 'font: bold 1; align: wrap 1;' )
currency_fmt = xlwt.XFStyle()
currency_fmt.num_format_str = "#,##0.00"
comma_fmt = xlwt.easyxf("", "#,###")
percent_fmt = xlwt.easyxf(num_format_str='0.00')
pct_pos_fmt = xlwt.easyxf( 'font: colour green;', num_format_str='0.00' )
pct_neg_fmt = xlwt.easyxf( 'font: colour red;',   num_format_str='0.00' )

def client(org):
    if org == "GPFXMT4":
        return "MT4"
    elif org == "MST":
        return "HF"
    elif org == "YIB":
        return "Metals"

    return org

def usage():
    print """usage: enter the year and month of the report you wish to generate in the form YYYY-MM"""

def vali_month(date):
    try:
        datetime.datetime.strptime(date, '%Y-%m')
    except ValueError:
        print "yields: invalid date! date should be of the format YYYY-MM"
        sys.exit(2)

def last_month(start):
    start_dt = datetime.datetime.strptime(start, "%Y-%m-%d").date()

    last_e = start_dt - datetime.timedelta(days=1)
    last_s = datetime.date(day=1, month=last_e.month, year=last_e.year)

    return (last_s, last_e)

def generate_report(start, end, lp, org):
    book = xlsxwriter.Workbook(gen_pathname(start, lp[0], org))
    sheet = book.add_worksheet("Integral")

    sheet.set_column(0, 12, 12)

    fmts = get_formats(book)

    # Set up title and headings
    image_path = dirs.logos + "Gleneagle.jpg"
    sheet.insert_image(0, 0, image_path, {"x_offset": 2, "y_offset": 1})

    date = datetime.datetime.strptime(start, "%Y-%m-%d")
    title = datetime.datetime.strftime(date, "FX Stats %B %Y")
    sheet.write(2, 0, title, fmts["title"])

    sheet.write(3, 0,  "Currency Pair",            fmts["heading"])
    sheet.write(3, 1,  "Total Pair Volume",        fmts["heading"])
    sheet.write(3, 2,  "Stream",                   fmts["heading"])
    sheet.write(3, 3,  "Stream Volume",            fmts["heading"])
    sheet.write(3, 4,  "Percentage of Total",      fmts["heading"])
    sheet.write(3, 5,  "Last Percentage of Total", fmts["heading"])
    sheet.write(3, 6,  "Percentage Change",        fmts["heading"])
    sheet.write(3, 7,  "Tickets",                  fmts["heading"])
    sheet.write(3, 8,  "Average Ticket Size",      fmts["heading"])
    sheet.write(3, 9,  "Rejections",               fmts["heading"])
    sheet.write(3, 10, "Rejections Percentage",    fmts["heading"])
    sheet.write(3, 11, "Rejections Sum",           fmts["heading"])
    sheet.write(3, 12, "Average Rejection Size",   fmts["heading"])

    # Collect the data
    conn, curr = utils.connect.psql()

    non = "non_" if org == "all" else ""

    q = "select * from {}mst_lp_report('{}', '{}', {})"
    q = q.format(non, start, end, lp[1])
    curr.execute(q)
    res = curr.fetchall()

    last_s, last_e = last_month(start)

    q = "select * from {}mst_lp_report('{}', '{}', {})"
    q = q.format(non, last_s, last_e, lp[1])
    curr.execute(q)
    res2 = curr.fetchall()

    row = 4
    for r in res:
        sheet.write(row, 0, r[0])
        sheet.write(row, 1, r[1], fmts["comma"])
        sheet.write(row, 2, r[3])

        if r[4]:
            sheet.write(row, 3, r[4], fmts["comma"])
            pct = 100 * float(r[4]) / float(r[1])
            sheet.write(row, 4, pct, fmts["pct"])
        else:
            pct = 0

        try:
            last = next(r2 for r2 in res2 if r2[0] == r[0] and
                                             r2[3] == r[3])

            if not last[4]: raise StopIteration

            last_pct = float(last[4]) / float(last[1]) * 100
            delta = pct - last_pct
            if delta > 0:
                delta_fmt = fmts["pct_pos"]
            elif delta == 0:
                delta_fmt = fmts["pct"]
            elif delta < 0:
                delta_fmt = fmts["pct_neg"]
            sheet.write(row, 5, last_pct, fmts["pct"])
            sheet.write(row, 6, delta,    delta_fmt)

        except StopIteration:
            if r[2]:
                sheet.write(row, 6, pct, fmts["pct_pos"])

        sheet.write(row, 7, r[5], fmts["comma"])

        if r[4]:
            avg = float(r[4]) / float(r[5])
            sheet.write(row, 8, avg, fmts["comma"])
        if r[6]:
            sheet.write(row, 9, r[6], fmts["comma"])
            pct = (float(r[6]) / float(r[6] + r[5])) * 100
            if pct >= REJECTION_TOLLERANCE:
                fmt = fmts["pct_neg"]
            else:
                fmt = fmts["pct"]
            sheet.write(row, 10, pct, fmt)
            sheet.write(row, 11, r[7], fmts["comma"])
            avg = float(r[7]) / float(r[6])
            sheet.write(row, 12, avg, fmts["comma"])

        row += 1

    # Create filter
    sheet.autofilter(3, 0, row - 1, 12)

    book.close()

def gen_pathname(date_str, lp, org):
    date = datetime.datetime.strptime(date_str, "%Y-%m-%d")

    path = "/home/adrian/host/Users/afreedman/Dropbox (Global Prime FX)/Global Prime FX/Ado/LP Monthly Stats/"
    path += datetime.datetime.strftime(date, "%Y%m/Integral/")

    if not os.path.exists(path): os.mkdir(path)

    if lp: path += lp + "-"
    if org == "*": org = "combined"
    path += org.upper() + ".xlsx"

    return path

def get_formats(book):
    formats = {}
    formats["title"] = book.add_format({"bold": True, "font_size": 14,
                                        "font_color": "black"})
    formats["heading"] = book.add_format({"bold": True,
                                          "text_wrap": True})
    formats["comma"] = book.add_format({"num_format": "#,##0"})
    formats["pct"] = book.add_format({"num_format": "0.00"})
    formats["pct_pos"] = book.add_format({"num_format": "0.00",
                                          "font_color": "green"})
    formats["pct_neg"] = book.add_format({"num_format": "0.00",
                                          "font_color": "red"})


    return formats

def replace_lp_names(res):
    lps = list({r[2] for r in res})
    ret = []
    for r in res:
        r2 = "LP" + str(lps.index(r[2])+1).zfill(2)
        trans = string.maketrans('','')
        nodigs = trans.translate(trans, string.digits)
        r3 = "Stream" + r[3].translate(trans, nodigs)
        ret.append((r[0], r[1], r2, r3, r[4], r[5], r[6], r[7]))

    return ret


def generate_all_report(start, end, org):
    book = xlsxwriter.Workbook(gen_pathname(start, None, org))
    sheet = book.add_worksheet("Monthly Report")

    sheet.set_column(0, 13, 12)

    fmts = get_formats(book)

    # Set up title and headings
    image_path = dirs.logos + "Gleneagle.jpg"
    sheet.insert_image(0, 0, image_path, {"x_offset": 2, "y_offset": 1})

    date = datetime.datetime.strptime(start, "%Y-%m-%d")
    title = datetime.datetime.strftime(date, "FX Stats %B %Y")
    sheet.write(2, 0, title, fmts["title"])

    sheet.write(3, 0,  "Currency Pair",            fmts["heading"])
    sheet.write(3, 1,  "Total Pair Volume",        fmts["heading"])
    sheet.write(3, 2,  "LP",                       fmts["heading"])
    sheet.write(3, 3,  "Stream",                   fmts["heading"])
    sheet.write(3, 4,  "Stream Volume",            fmts["heading"])
    sheet.write(3, 5,  "Percentage of Total",      fmts["heading"])
    sheet.write(3, 6,  "Last Percentage of Total", fmts["heading"])
    sheet.write(3, 7,  "Percentage Change",        fmts["heading"])
    sheet.write(3, 8,  "Tickets",                  fmts["heading"])
    sheet.write(3, 9,  "Average Ticket Size",      fmts["heading"])
    sheet.write(3, 10, "Rejections",               fmts["heading"])
    sheet.write(3, 11, "Rejections Percentage",    fmts["heading"])
    sheet.write(3, 12, "Rejections Sum",           fmts["heading"])
    sheet.write(3, 13, "Average Rejection Size",   fmts["heading"])

    # Collect the data
    conn, curr = utils.connect.psql()

    if org == "all":
        prefix = "non_mst" 
    elif org == "HF":
        prefix = "mst"
    elif org == "*":
        prefix = "all"

    q = "select * from {}_lp_report('{}', '{}', '{}')"
    q = q.format(prefix, start, end, "%")
    curr.execute(q)
    res = replace_lp_names(curr.fetchall())

    last_s, last_e = last_month(start)

    q = "select * from {}_lp_report('{}', '{}', '{}')"
    q = q.format(prefix, last_s, last_e, "%")
    curr.execute(q)
    res2 = replace_lp_names(curr.fetchall())

    row = 4
    for r in res:
        sheet.write(row, 0, r[0])
        sheet.write(row, 1, r[1], fmts["comma"])
        sheet.write(row, 2, r[2])
        sheet.write(row, 3, r[3])

        if r[4]:
            sheet.write(row, 4, r[4], fmts["comma"])
            pct = 100 * float(r[4]) / float(r[1])
            sheet.write(row, 5, pct, fmts["pct"])
        else:
            pct = 0

        try:
            last = next(r2 for r2 in res2 if r2[0] == r[0] and
                                             r2[2] == r[2] and
                                             r2[3] == r[3])

            if not last[4]: raise StopIteration

            last_pct = float(last[4]) / float(last[1]) * 100
            delta = pct - last_pct
            if delta > 0:
                delta_fmt = fmts["pct_pos"]
            elif delta == 0:
                delta_fmt = fmts["pct"]
            elif delta < 0:
                delta_fmt = fmts["pct_neg"]
            sheet.write(row, 6, last_pct, fmts["pct"])
            sheet.write(row, 7, delta,    delta_fmt)

        except StopIteration:
            if r[2]:
                sheet.write(row, 7, pct, fmts["pct_pos"])

        sheet.write(row, 8, r[5], fmts["comma"])

        if r[4]:
            avg = float(r[4]) / float(r[5])
            sheet.write(row, 9, avg, fmts["comma"])
        if r[6]:
            sheet.write(row, 10, r[6], fmts["comma"])
            pct = (float(r[6]) / float(r[6] + r[5])) * 100
            if pct >= REJECTION_TOLLERANCE:
                fmt = fmts["pct_neg"]
            else:
                fmt = fmts["pct"]
            sheet.write(row, 11, pct, fmt)
            sheet.write(row, 12, r[7], fmts["comma"])
            avg = float(r[7]) / float(r[6])
            sheet.write(row, 13, avg, fmts["comma"])

        row += 1

    # Create filter
    sheet.autofilter(3, 0, row - 1, 13)

    book.close()

def main(argv):
    global lps, curr
    # Check arguments
    if len(argv) != 1:
        usage()
        sys.exit(2)

    start = argv[0] + "-01"
    end = argv[0] + "-" + str(calendar.monthrange(int(argv[0][:4]),
                                                  int(argv[0][5:]))[1])

    conn, cursor = utils.connect.psql()

    q = "select distinct l.name, l.id from lps l join takers a on l.id=a.lp join trades t on t.id=a.trade where t.trade_date between '{0}' and '{1}' order by 1".format(start, end)
    cursor.execute(q)

    for lp in cursor.fetchall():
        print lp
        generate_report(start, end, lp, "all")
        # generate_report(start, end, lp, "HF")

    generate_all_report(start, end, "all")
    # generate_all_report(start, end, "HF")
    generate_all_report(start, end, "*")

    conn.close()




if __name__ == "__main__":
    main(sys.argv[1:])
