import datetime
import directories as dirs
import pysftp
import utils.date as dd

MONDAY = 0

def connect():
    host = "sftp.primebroker.com"
    user = "inf_gleneagle"
    pwrd = "S#H7wm@3"

    return pysftp.Connection(host=host, username=user, password=pwrd)

def get_one(srv, date):
    path = "./FromBNPP/"
    name = "GleneagleRollCost_{}.csv.pgp".format(date)
    
    rolls = dirs.tmp + "/rolls/encrypted"

    srv.get(path + name, rolls + "/{}.csv.pgp".format(date))

def get(get_all=False):
    srv = connect()

    yesterday = dd.yesterday()

    count = 0

    try:
        while 1:
            date = yesterday.strftime("%Y%m%d")
            get_one(srv, date)
            count += 1

            if not get_all:
                break

            yesterday = dd.yesterday(yesterday)                

    except IOError:
        if get_all:
            print "Pulled {} files".format(count)

    srv.close()

# get(get_all=True)