import datetime
import directories as dirs
import glob
import gnupg
import os
import sys

def init():
    gpg = gnupg.GPG()
    gpg.import_keys(open(dirs.assets + "/bnpges.key").read())
    gpg.import_keys(open(dirs.assets + "/bnpgesprivate.key").read())

    return gpg

def decrypt():
    gpg = init()

    rolls = dirs.tmp + "/rolls"
    decrypt = rolls + "/decrypted"
    encrypt = rolls + "/encrypted"

    files = glob.glob(encrypt + "/*")
    count = 0

    for r in files:
        e_name = r.split("/")[-1]
        d_name = e_name.split(".pgp")[0]

        with open(r, 'rb') as f:
            status = gpg.decrypt_file(f, passphrase='Gleneagle1',
                                      output=decrypt + "/" + d_name)

        if status.ok:
            os.remove(r)
            count += 1
        else:
            print d_name + " unable to be undecrypted"

    print "Decrypted {} out of {} file(s)".format(count, len(files))

# decrypt()