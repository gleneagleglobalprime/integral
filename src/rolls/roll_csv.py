import csv
import datetime
import directories as dirs
import glob
import os
import sys

def to_csv():
    rolls = dirs.tmp + "/rolls/decrypted"

    files = glob.glob(rolls + "/*")
    data = ""

    for f in files:
        out = dirs.tmp + "/rolls/csv"
        out += "/" + f.split("/")[-1]

        with open(f, "rb") as infile, open(out, "wb") as outfile:
            reader = csv.reader(infile)
            # Skip the header
            next(reader, None)
            
            writer = csv.writer(outfile)
            for row in reader:
                date = datetime.datetime.strptime(row[1], "%m-%d-%Y").\
                                         strftime("%Y-%m-%d")
                pair = row[3] + "/" + row[4]

                writer.writerow([date, pair, row[2], row[8], row[14]])

            os.remove(f)

# to_csv()