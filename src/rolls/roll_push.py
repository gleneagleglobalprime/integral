import csv
import utils.date as dd
import datetime
import directories as dirs
import glob
import imp
import json
import os
import utils.connect
import sys

DATE   = 1
NEUM   = 3
DENOM  = 4
NEAR   = 2
FAR    = 8
COST   = 14

def load(curr):
    rolls = set()

    q = "select near_tkt, far_tkt from rolls"
    curr.execute(q)
    for r in curr.fetchall(): rolls.add(r[0] + r[1])

    return rolls

def push_BNP(curr, rolls):
    csvs = dirs.tmp + "/rolls/decrypted"

    files = glob.glob(csvs + "/*")

    for f in files:
        with open(f, "rb") as infile:
            reader = csv.reader(infile)
            # Skip the header
            next(reader, None)

            for row in reader:
                if row[NEAR] + row[FAR] in rolls:
                    continue
            
                date = datetime.datetime.strptime(row[DATE], "%m-%d-%Y").\
                                         strftime("%Y-%m-%d")
                pair = row[NEUM] + "/" + row[DENOM]

                q = "insert into rolls values (DEFAULT, '{}', '{}', " +\
                    "'{}', '{}', {}, '{}', null)"
                q = q.format(date, pair, row[NEAR], row[FAR], row[COST],
                             'bnp')
                curr.execute(q)

def push_insto(curr, rolls):
    path = dirs.eugene + "/Institutional Rolls/"

    yesterday = dd.yesterday(datetime.datetime.today())
    date = yesterday.strftime("%Y%m%d")

    f = path + date + ".json"

    with open(f, "rb") as infile:
        insto = json.load(infile)

    for i in insto:
        if i['far_tkt'] + i['near_tkt'] in rolls:
            continue

        q = "insert into rolls values (DEFAULT, '{}', '{}', '{}', '{}', " +\
            "{}, '{}', '{}')"
        q = q.format(date, i['c_pair'], i['near_tkt'], i['far_tkt'], 
            i['cost'], 'insto', i['portfolio'].replace("IFX", ""))
        curr.execute(q)

    # Put file in old folder after data has been pushed to db
    old = path + "old/"
    os.rename(f, old + date + ".json")

def push():
    conn, curr = utils.connect.psql()
    rolls = load(curr)

    push_BNP(curr, rolls)
    push_insto(curr, rolls)

    conn.commit()
    conn.close()

# push_insto(None, [])
