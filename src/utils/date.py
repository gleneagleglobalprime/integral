import calendar
import datetime

MONDAY   = 0
FRIDAY   = 4
SATURDAY = 5
SUNDAY   = 6
MONTH    = {v: k for k,v in enumerate(calendar.month_abbr)}

# Returns the previous weekday from today if no arg supplied, else
# returns the previous weekday from the given date
def yesterday(date=None):
    change = 3 if date else 2
    if date:
        change = 3
    else:
        change = 2
        date = datetime.datetime.today()
    delta = change if date.weekday() == MONDAY else 1
    return date - datetime.timedelta(delta)

def next_weekday(date, n):
    date += datetime.timedelta(n)

    if date.weekday() == SATURDAY:
        delta = 2
    elif date.weekday() == SUNDAY:
        delta = 1
    else:
        delta = 0

    date += datetime.timedelta(delta)

    return date

def rename_date(date_str):
    parts = date_str.split("-")
    if len(parts) == 2:
        return parts[1] + "-" + str(MONTH[parts[0]]).zfill(2)
    else:
        return parts[2] + "-" + str(MONTH[parts[1]]).zfill(2) + "-" + parts[0]