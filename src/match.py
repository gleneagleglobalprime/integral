import csv
from datastructs import *
import time
from operator import itemgetter

matches = []

unmatched_takers = []
unmatched_makers = []

def out(f_dir):
    # Open the file and filter all takers
    trades = csv.reader(open(f_dir, 'rU'), delimiter=",", quotechar='"')
    # unmatched_takers = [t for t in trades if t[STATUS] == "C" and t[MKRTKR] == "Taker"]
    tkrs = {}
    for t in trades:
        if t[STATUS] == "C" and t[MKRTKR] == "Taker":
            if t[COVERED_TRADE_ID] in tkrs:
                tkrs[t[COVERED_TRADE_ID]].append(t)
            else:
                tkrs[t[COVERED_TRADE_ID]] = [t]

    # Reopen the file and do the same for the makers
    trades = csv.reader(open(f_dir, 'rU'), delimiter=",", quotechar='"')
    unmatched_makers = [m for m in trades if m[STATUS] == "C" and m[MKRTKR] == "Maker"]
    
    # sorted_takers = sorted(unmatched_takers, key=itemgetter(COVERED_TRADE_ID))
    sorted_makers = sorted(unmatched_makers, key=itemgetter(TRADE_ID))

    count = 0
    # Match maker to takers and store in a dict
    for maker in sorted_makers:
        # Get a list of the trade's taker trade ids
        trade_ids = maker[MAKER_REF_ID].split("-")

        # Find the associated takers. There is a one-to-one
        # relationship between the each trade id and each maker
        # ref id so there is at least one match and no duplicates
        # takers = [t for t in sorted_takers if t[COVERED_TRADE_ID] == \
        #                                       maker[TRADE_ID]]
        takers = tkrs[maker[TRADE_ID]]
        matches.append([maker, takers])

        count += 1

    # print len(sorted_takers), len(sorted_makers)
    # print sorted_takers
    # unmatched.out("UnmatchedTakers", sorted_takers, [])

    return matches

