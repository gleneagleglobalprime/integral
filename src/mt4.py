#import utils.date as dd
#import utils.connect as uc
#import datetime

#PAIR  = 0
#SWAP  = 1
#DENOM = 2

#EXP = 0
#INC = 1

#curr = uc.mysql()

#todate = datetime.datetime.today()
#yesterdate = dd.yesterday(todate)

#today = todate.strftime("%Y-%m-%d")
#yesterday = yesterdate.strftime("%Y-%m-%d")

## rec query
#q = "select t.SYMBOL, t.SWAPS, u.CURRENCY from MT4_TRADES t join " +\
#    "MT4_USERS u on u.LOGIN=t.LOGIN where t.CLOSE_TIME>='{}' AND " +\
#    "t.CLOSE_TIME<'{}' and t.SWAPS<>0"
## swaps query
## q = "SELECT MT4_TRADES.LOGIN, MT4_TRADES.SYMBOL, MT4_TRADES.CMD, -swaps as 'swap_income', MT4_TRADES.CLOSE_TIME, MT4_USERS.CURRENCY FROM MT4_TRADES LEFT JOIN MT4_USERS ON MT4_TRADES.LOGIN = MT4_USERS.LOGIN LEFT JOIN     (     SELECT SUBSTRING(MT4_TRADES.COMMENT, LOCATE('#', MT4_TRADES.COMMENT) + 1) as 'Ticket', MT4_TRADES.LOGIN as 'AgentCode', MT4_USERS.GROUP as 'AgentGroup'     FROM MT4_TRADES      LEFT JOIN MT4_USERS ON MT4_TRADES.LOGIN = MT4_USERS.LOGIN     WHERE CMD = 6 AND Date(CLOSE_TIME) = {} AND MT4_TRADES.COMMENT LIKE 'agent %'     ) as CommissionBucket ON CommissionBucket.Ticket = MT4_TRADES.TICKET WHERE      Date(CLOSE_TIME) = {} AND      (CMD = '0' OR CMD = '1') AND      MT4_TRADES.LOGIN = MT4_USERS.LOGIN AND      NOT (MT4_TRADES.Comment like 'Vol:%;M.Order:%' OR MT4_TRADES.Comment like 'close hedge by%') AND     NOT AgentGroup like 'GP-MAM-SPD%' order by MT4_TRADES.CLOSE_TIME limit 10"
#q = q.format('2015-06-01', '2015-06-02')

#curr.execute(q)
#swap = curr.fetchall()
#print len(swap)

#for s in swap:
#    print s

## Collect unique currencies
#currency_map = {c[2]: None for c in swap}
#currency = tuple(currency_map.keys())

#conn, curr = uc.psql()

#q = "select code, rate from eod_rates where date='{}' and code in {}"
#q = q.format("2015-06-01", currency)
#curr.execute(q)

#eod = {e[0]: e[1] for e in curr.fetchall()}

#roll = {}
#for s in swap:
#    if s[PAIR] not in roll:
#        roll[s[PAIR]] = [0, 0]

#    aud_swap = (s[SWAP] / eod[s[DENOM]]) / eod['AUD']
#    i = EXP if aud_swap < 0 else INC
#    roll[s[PAIR]][i] += aud_swap

#for r in sorted(roll):
#    print r, roll[r]