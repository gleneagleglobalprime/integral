import csv
import sys
from datastructs import *

def out(f_dir):
    # Open the file and filter all rejected takers on the taker side
    trades = csv.reader(open(f_dir, 'rU'), delimiter=",", quotechar='"')
    r_takers = [r for r in trades if r[STATUS] == "R" and r[MKRTKR] == "Taker"]

    # Open the file and filter all rejected makers on the taker side
    trades = csv.reader(open(f_dir, 'rU'), delimiter=",", quotechar='"')
    r_makers = [r for r in trades if r[STATUS] == "R" and r[MKRTKR] == "Maker"]

    # Open the file and filter all completed makers on the maker side
    trades = csv.reader(open(f_dir, 'rU'), delimiter=",", quotechar='"')
    c_makers = [r for r in trades if r[STATUS] == "C" and r[MKRTKR] == "Maker"]

    rejections = []
    for m in r_makers:
        # Try finding taker to match the maker
        takers = []
        try:
            while True:
                taker = next(t for t in r_takers
                             if m[TRADE_ID] == t[COVERED_TRADE_ID])
                r_takers.remove(taker)
                takers.append(taker)

        except StopIteration:
            pass

        # Add taker maker pair to rejections
        if takers:
            for t in takers:
                rejections.append((m, t))
        else:
            # No taker found to match maker. Still add to rejections
            rejections.append((m, None))

    # Match remaining rejected takers with completed makers
    for t in r_takers:
        try:
            maker = next(m for m in c_makers
                         if m[TRADE_ID] == t[COVERED_TRADE_ID])
            rejections.append((maker, t))
        except StopIteration:
            pass

    print "num rejections", len(rejections)

    return rejections